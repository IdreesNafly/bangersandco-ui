import React, { Component } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import Homepage from "./Pages/Homepage";
import SignUp from "./Pages/SignUp";
import SignIn from "./Pages/Login";
import Rental from "./Pages/Rental";
import AboutUs from "./Pages/About";
import ContactUs from "./Pages/ContactUs";
import Fleet from "./Pages/Fleet";
import Intro from "./Components/intro";
import UserDashboard from "./Pages/UserDashboard/UserDashboardContainer";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import MainNavbar from "./Components/MainNavbar";
import FooterPage from "./Components/Footer";
import Modal from "./Components/Modal";
import { AuthProvider } from "./AuthContext";
import AdminDashboard from "./Pages/AdminDashboard/AdminDashboard";
import AdminContainer from "./Pages/AdminDashboard/AdminContainer";
import ManageUsers from "./Pages/AdminDashboard/ManageUsers";
import ManageVehicles from "./Pages/AdminDashboard/ManageVehicles";
import ManageEquipments from "./Pages/AdminDashboard/ManageEquipments";
import ManageBooking from "./Pages/AdminDashboard/ManageBookings";
import Checkout from "./Components/Checkout";
import CheckoutPage from "./Components/CheckoutPage";
import { ROLE } from "./Utils";
import ChatBot from "react-simple-chatbot";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: "",
      isAuthenticated: false
    };
  }
  componentDidMount() {
    const user = localStorage.getItem("UserRole");
    const token = localStorage.getItem("JWTToken");

    this.setState({
      role: user,
      token: token
    });
  }
  render() {
    let userRole = this.state.role;
    let bool = true;

    if (userRole === "Customer" && userRole == null) {
      bool = true;
    } else if (userRole === "Admin") {
      bool = false;
    }
    return (
      <Router>
        <AuthProvider>
          <Route exact path="/SignIn" component={SignIn} />
          <Route exact path="/SignUp" component={SignUp} />
          {bool ? <MainNavbar /> : ""}
          <Switch>
            <Route exact path="/" component={Homepage} />
            <Route path="/Rental" component={Rental} />
            <Route path="/About" component={AboutUs} />
            <Route path="/Contact" component={ContactUs} />
            <Route path="/Fleet" component={Fleet} />
            <Route path="/intro" component={Intro} />
            <Route path="/logout" component={Intro} />
            <Route path="/checkout" component={CheckoutPage} />
            <Route path="/userdashboard" component={UserDashboard} />
            <Route path="/userdashboard/modal/::id" component={Modal} />
          </Switch>
          <Route path="/admindashboard" component={AdminContainer} />
          <Route
            path="/admindashboard/manageequipments/modal"
            component={Modal}
          />
        </AuthProvider>
        <ChatBot
          floating
          steps={[
            {
              id: "1",
              message: "What is your name?",
              trigger: "2"
            },
            {
              id: "2",
              user: true,
              trigger: "3"
            },
            {
              id: "3",
              message: "Hi {previousValue}, nice to meet you!",
              end: true
            }
          ]}
        />
      </Router>
    );
  }
}

export default App;
