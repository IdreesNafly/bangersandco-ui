import React, { Component } from "react";
import { Redirect } from "react-router-dom";

const DEFAULT_STATE = { isAuth: false, logout: null };

const AuthContext = React.createContext(DEFAULT_STATE);

class AuthProvider extends Component {
  constructor() {
    super();
    this.state = {
      isAuth: false,
      email: null
    };
    this.logout = this.logout.bind(this);
  }

  logout = () => {
    this.setState({
      isAuth: false
    });
    localStorage.clear();
    return <Redirect to="/" />;
  };

  componentDidMount() {
    const EMAIL = localStorage.getItem("email");
    const TOKEN = localStorage.getItem("JWTToken");
    const ROLE = localStorage.getItem("UserRole");
    if (EMAIL && TOKEN) {
      this.setState({
        isAuth: true,
        email: EMAIL,
        token: TOKEN,
        role: ROLE
      });
    }
  }

  render() {
    return (
      <AuthContext.Provider
        value={{
          isAuth: this.state.isAuth,
          logout: this.logout,
          email: this.state.email,
          token: this.state.token,
          role: this.state.role
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}

const AuthConsumer = AuthContext.Consumer;

export { AuthProvider, AuthConsumer };
