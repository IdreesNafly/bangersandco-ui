import React, { Component } from "react";
import axios from "axios";
import { Divider, Result, Button } from "antd";
import { differenceInHours } from "date-fns";
import styled from "styled-components";
import { Link } from "react-router-dom";

const Title = styled.span`
  font-size: 1.3rem;
  font-weight: bold;
`;

class BookingComponent extends Component {
  state = {
    vehicle: {},
    equipmentsData: []
  };

  componentDidMount() {
    const token = localStorage.getItem("JWTToken");
    this.setState({
      token: token
    });
    this.getEquipmentDetails(token);
    this.getVehicleById(token);
  }

  render() {
    const propstate = this.props.state;
    var result = differenceInHours(propstate.returnDate, propstate.startDate);
    return (
      <div style={{ display: "flex", flex: 1 }}>
        {propstate.isConfirmed === false ? (
          <div
            style={{
              padding: "2rem",
              display: "flex",
              flexDirection: "column",
              flex: 1
            }}
          >
            <Title>Please Confirm your booking Details</Title>

            <div style={{ display: "flex", flexDirection: "row", flex: 1 }}>
              <div
                style={{ display: "flex", flexDirection: "column", flex: 1 }}
              >
                <Divider orientation="left">
                  {"Booked Vehicle:  " +
                    this.state.vehicle.vehicleMake +
                    " " +
                    this.state.vehicle.vehicleModel}
                </Divider>

                <Divider orientation="left">
                  {"Cost Per Day: " + "LKR " + this.state.vehicle.vehicleRate}
                </Divider>
                <Divider orientation="left">Equipments Added</Divider>
                <div>
                  {this.state.equipmentsData ? (
                    this.state.equipmentsData.map(equipment => {
                      return <div>{equipment.equipmentName}</div>;
                    })
                  ) : (
                    <div>No Equipments Added</div>
                  )}
                </div>
              </div>
              <Divider type="vertical" style={{ height: "100%" }} />
              <div
                style={{ display: "flex", flexDirection: "column", flex: 1 }}
              >
                <Divider orientation="left">
                  {"Booking Date: " + String(propstate.startDate)}
                </Divider>
                <Divider orientation="left">
                  {"Return Date:  " + String(propstate.returnDate)}
                </Divider>
                <Divider orientation="left">
                  {"Total Booking Hours: " + result + " Hours"}
                </Divider>
                <Divider orientation="left">
                  {"Total Booking Amount: " +
                    "LKR " +
                    this.calculateTotalAmount()}
                </Divider>
              </div>
            </div>
          </div>
        ) : (
          <Result
            status="success"
            title="Your Vehicle has been booked Successfully"
            subTitle="Thank you for Choosing Bangers and Co as your go to Vehicle Rental Company.Your Booking Reference Number is your email address. Enjoy and have a safe ride"
            extra={[
              <Link to="/">
                <Button type="primary" key="console">
                  Go Home
                </Button>
              </Link>,
              <Link to="/Rental">
                <Button key="buy">Make Another Booking</Button>
              </Link>
            ]}
          />
        )}
      </div>
    );
  }

  getVehicleById = token => {
    let self = this;
    axios
      .get(
        `http://localhost:8080/api/vehicle/getvehicle/${this.props.state.selectedVehicle}`,
        {
          headers: {
            Authorization: "Bearer " + token
          }
        }
      )
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          self.setState({
            vehicle: res.data
          });
        }
      });
  };

  getEquipmentDetails(token) {
    let ids = this.props.state.SelectedEquipments;
    let equipments = [];
    if (ids && ids.length > 0) {
      ids.map(id => {
        axios
          .get(`http://localhost:8080/api/equipments/getbyid/${id}`, {
            headers: {
              Authorization: "Bearer " + token
            }
          })
          .then(res => {
            if (res.status === 200) {
              equipments.push(res.data);
            }
          });
      });

      let equipmentNames = equipments.map(equipment => equipment.equipmentName);
      console.log(equipmentNames);
      this.setState({
        equipmentsData: equipments
      });
    }
  }

  calculateTotalAmount() {
    const propstate = this.props.state;
    let totalamount = 0;
    if (propstate && this.state) {
      let rate = this.state.vehicle.vehicleRate;
      let ratePerhour = rate / 10;
      var diff = differenceInHours(propstate.returnDate, propstate.startDate);
      let total = ratePerhour * diff;
      var equipmentrates = this.state.equipmentsData.map(
        equipment => equipment.costPerHour
      );
      let eqsum = 0;
      let tot = 0;
      if (equipmentrates.length > 0) {
        eqsum = equipmentrates.reduce((result, number) => result + number);
        tot = eqsum * diff;
      }

      totalamount = total + tot;
    }
    return totalamount;
  }
}

export default BookingComponent;
