import React, { Component } from "react";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";
import "../../Stylesheets/stepperstyles.css";
import { addWeeks, setHours, setMinutes } from "date-fns";

class SelectDate extends Component {
  constructor(props) {
    super(props);
    // this.handleStartDate=this.handleStartDate.bind(this);
    // this.handleEndDate=this.handleEndDate.bind(this);
  }

  render() {
    const { state, handleStartDate, handleEndDate } = this.props;
    return (
      <div
        style={{ display: "flex", flexDirection: "column", padding: "2rem" }}
      >
        <div style={style}>Start Date</div>
        <DatePicker
          selected={state.startDate}
          minDate={state.startDate}
          startDate={state.startDate}
          endDate={state.returnDate}
          onChange={date => handleStartDate(date)}
          onSelect={date => handleStartDate(date)}
          showTimeSelect
          selectsStart
          minTime={setHours(setMinutes(new Date(), 0), 8)}
          maxTime={setHours(setMinutes(new Date(), 0), 17)}
          timeFormat="HH:mm"
          timeIntervals={60}
          timeCaption="time"
          dateFormat="MMMM d, yyyy h:mm aa"
        />
        <div style={style}>End Date</div>
        <DatePicker
          selected={state.returnDate}
          onSelect={date => handleEndDate(date)}
          maxDate={addWeeks(state.startDate, 2)}
          startDate={state.startDate}
          minDate={state.startDate}
          endDate={state.returnDate}
          minTime={setHours(setMinutes(new Date(), 0), 8)}
          maxTime={setHours(setMinutes(new Date(), 0), 17)}
          selectsEnd
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={60}
          timeCaption="time"
          dateFormat="MMMM d, yyyy h:mm aa"
        />
      </div>
    );
  }
}
const style = {
  color: "black",
  fontSize: "1.2rem",
  padding: "1rem",
  fontVariantCaps: "small-caps",
  fontWeight: 500
};

export default SelectDate;
