import React, { Component } from "react";
import { Select } from "antd";

const style = {
  color: "black",
  fontSize: "1.2rem",
  padding: "1rem",
  fontVariantCaps: "small-caps",
  fontWeight: 500
};
class SelectEquipment extends Component {
  state = {
    selectedItems: []
  };

  handleChange = value => {
    console.log(value);
    this.props.action(value);
  };

  componentDidMount() {}

  render() {
    const { equipmentData } = this.props;
    //const OPTION = equipmentData;
    //const filteredOptions = OPTION.filter(o => !selectedItems.includes(o));
    return (
      <div
        style={{
          display: "flex",
          width: "50%",
          alignSelf: "center",
          flexDirection: "column",
          alignItems: "center"
        }}
      >
        <div style={style}>Select Additional Equipments for your booking</div>
        <Select
          mode="multiple"
          placeholder="Choose Additional equipments"
          onChange={e => {
            this.handleChange(e);
          }}
          style={{ width: "100%" }}
          allowClear={true}
          optionLabelProp="label"
        >
          {equipmentData.map(item => (
            <Select.Option
              key={item.equipmentId}
              value={item.equipmentId}
              label={item.equipmentName}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  flexDirection: "row"
                }}
              >
                <div>{item.equipmentName}</div>
                <div>{"LKR: " + item.costPerHour}</div>
              </div>
            </Select.Option>
          ))}
        </Select>
      </div>
    );
  }
}

export default SelectEquipment;
