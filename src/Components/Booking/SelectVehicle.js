import React, { Component } from "react";
import { Card } from "antd";
import "../../Stylesheets/stepperstyles.css";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import "react-datepicker/dist/react-datepicker.css";
import pic from "../../altpic.jpg";
import { MDBBtn } from "mdbreact";
import { differenceInYears } from "date-fns";
import cogoToast from "cogo-toast";

const { Meta } = Card;

const style = {
  color: "black",
  fontSize: "1.2rem",
  padding: "1rem",
  fontVariantCaps: "small-caps",
  fontWeight: 500
};
class Selectvehicle extends Component {
  constructor(props) {
    super(props);
    this.handleOnClick = this.handleOnClick.bind(this);
  }

  componentDidMount() {
    this.formatVehicleData();
  }
  render() {
    const { vehicleData, userData } = this.props;
    let data = vehicleData;
    let arr = [];
    if (userData && data) {
      let dateAge = new Date(userData).toLocaleDateString();
      let now = new Date().toLocaleDateString();
      let ageYears = differenceInYears(new Date(now), new Date(dateAge));
      if (ageYears < 25) {
        data.map(datum => {
          if (datum.vehicleType === "TOWNCAR") {
            arr.push(datum);
          }
        });
        console.log(arr);
        cogoToast.info(
          "You can only book Town Cars as your below the age of 25. Thank you"
        );
        data = arr;
      }
    }

    return (
      <div className="cardWrapper">
        {data && data.length > 0 ? (
          data.map(datum => {
            return (
              <Card
                hoverable
                style={{
                  width: 275,
                  minWidth: 275,
                  margin: "0 1rem 1rem 0"
                }}
                actions={[
                  <MDBBtn
                    gradient="blue"
                    onClick={e => {
                      this.handleOnClick(datum.vehicleID);
                    }}
                  >
                    Select Vehicle
                  </MDBBtn>
                ]}
                cover={<img alt={pic} src={datum.imagePath} />}
              >
                <Meta title={datum.vehicleMake + " " + datum.vehicleModel} />
                <div>
                  <div className="cardDesc">
                    <div className="descItem">
                      {"Transmission: " + datum.transmission}
                    </div>
                    <div className="descItem">{"Fuel: " + datum.fuelType}</div>
                    <div className="descItem">
                      {"Mileage " + datum.mileage + " KM"}
                    </div>
                    <div className="descItem">
                      {"Price: " + "LKR " + datum.vehicleRate + " Per KM"}
                    </div>
                  </div>
                  <div className="cardBtnWrapper"></div>
                </div>
              </Card>
            );
          })
        ) : (
          <div style={style}>No Data</div>
        )}
      </div>
    );
  }

  formatVehicleData() {
    const { userData, vehicleData } = this.props;
    console.log(userData);
    let arr = [];
    if (userData && vehicleData) {
      console.log(userData, vehicleData);
      let dateAge = new Date(userData).toLocaleDateString();
      let now = new Date().toLocaleDateString();
      let ageYears = differenceInYears(new Date(now), new Date(userData));
      console.log(dateAge, now, ageYears);
      if (ageYears < 25) {
        vehicleData.map(datum => {
          if (datum.vehicleType === "TOWNCAR") {
            arr.push(datum);
          }
        });
      }
      this.setState({
        vehicleDataUpdated: arr
      });
    }
  }

  handleOnClick(data) {
    if (data) {
      this.props.action(data);
    }
  }
}

export default Selectvehicle;
