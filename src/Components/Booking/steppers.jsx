import React, { Component } from "react";
import { Steps, Button, Modal, message, Alert } from "antd";
import "../../Stylesheets/stepperstyles.css";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import "react-datepicker/dist/react-datepicker.css";
import SelectDate from "../Booking/SelectDate";
import Selectvehicle from "./SelectVehicle";
import SelectEquipment from "./SelectEquipment";
import BookinConfirmation from "./BookingConfirmation";
import { add, addHours } from "date-fns";
import axios from "axios";
import { Link } from "react-router-dom";
import { ExclamationCircleTwoTone } from "@ant-design/icons";
const { confirm } = Modal;
const { Step } = Steps;

const stepTypes = {
  STEPS_VEHICLE: "STEPS_VEHICLE",
  STEPS_DATE: "STEPS_DATE",
  STEPS_EQUIPMENT: "STEPS_EQUIPMENT",
  STEPS_CONFIRMATION: "STEPS_CONFIRMATION"
};

const steps = [
  {
    title: "Select Vehicle",
    content: stepTypes.STEPS_VEHICLE
  },
  {
    title: "Select Date and Time",
    content: stepTypes.STEPS_DATE
  },
  {
    title: "Select Additional Equipment",
    content: stepTypes.STEPS_EQUIPMENT
  },
  {
    title: "Booking Confirmation",
    content: stepTypes.STEPS_CONFIRMATION
  }
];

class CarBooking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      startDate: new Date(),
      returnDate: null,
      selectedVehicle: null,
      SelectedDate: {},
      SelectedEquipments: [],
      isConfirmed: false,
      isError: false
    };
    this.handleSubmitVehicle = this.handleSubmitVehicle.bind(this);
    this.handleSubmitEquipments = this.handleSubmitEquipments.bind(this);
    this.handleSubmitDates = this.handleSubmitDates.bind(this);
    this.handleStartDate = this.handleStartDate.bind(this);
    this.handleEndDate = this.handleEndDate.bind(this);
    this.returnDate = this.returnDate.bind(this);
  }

  componentDidMount() {
    this.returnDate();
  }
  componentDidUpdate(prevProps, prevState) {}

  returnDate = () => {
    this.setState({
      returnDate: addHours(this.state.startDate, 5)
    });
  };
  handleStartDate(date) {
    this.setState({
      startDate: date
    });
    console.log("object", this.state.startDate);
    this.returnDate();
  }
  handleEndDate(date) {
    this.setState({
      returnDate: date
    });
  }
  handleSubmitVehicle(value) {
    this.setState({
      selectedVehicle: value
    });
    if (this.state.selectedVehicle) {
      console.log(this.state.selectedVehicle, "Vehicle");
      this.next();
    }
  }

  handleSubmitDates(value) {
    this.setState({
      SelectedDate: value
    });
    if (this.state.SelectedDate) {
      console.log(this.state.SelectedDate, "dates");
      this.next();
    }
  }

  handleSubmitEquipments(value) {
    this.setState({
      SelectedEquipments: value
    });
    if (this.state.SelectedEquipments) {
    }
    // console.log(this.state.selectedVehicle);
    // console.log(this.state.startDate, this.state.returnDate);
    // console.log(this.state.SelectedEquipments);
  }

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  renderVehicle = () => {
    const { vehicleData, userData } = this.props;
    return (
      <Selectvehicle
        state={this.state}
        vehicleData={vehicleData}
        action={this.handleSubmitVehicle}
        userData={userData}
      />
    );
  };

  renderConfirmation = () => {
    return <BookinConfirmation state={this.state} />;
  };

  renderDate = () => {
    return (
      <SelectDate
        state={this.state}
        action={this.handleSubmitDates}
        handleStartDate={this.handleStartDate}
        handleEndDate={this.handleEndDate}
      />
    );
  };

  renderEquipment = () => {
    const { equipmentData } = this.props;
    return (
      <SelectEquipment
        state={this.state}
        equipmentData={equipmentData}
        action={this.handleSubmitEquipments}
      />
    );
  };

  renderContent = () => {
    const { current } = this.state;
    const { STEPS_VEHICLE, STEPS_CONFIRMATION, STEPS_EQUIPMENT } = stepTypes;
    let componentToRender = null;

    switch (steps[current].content) {
      case STEPS_VEHICLE:
        componentToRender = this.renderVehicle();
        break;
      case STEPS_CONFIRMATION:
        componentToRender = this.renderConfirmation();
        break;
      case STEPS_EQUIPMENT:
        componentToRender = this.renderEquipment();
        break;
      default:
        componentToRender = this.renderDate();
    }

    return componentToRender;
  };

  render() {
    const { current } = this.state;
    const errormsg = this.state.ErrorMsg;
    return (
      <div className="wrapper">
        <Steps current={current}>
          {steps.map(item => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
        <div className="steps-content">
          {/* {steps[current].content(this.state)} */
          this.renderContent()}
        </div>
        <div className="steps-action">
          {current < steps.length - 1 && (
            <Button type="primary" onClick={() => this.next()}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button
              type="primary"
              onClick={() => {
                this.submitAddBooking();
              }}
              // onClick={() => message.success("Processing complete!")}
            >
              Make Booking
            </Button>
          )}
          {current > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
        </div>
        <div>
          {this.state.isError ? (
            <Alert
              message="Error"
              description={this.state.ErrorMsg}
              type="error"
              showIcon
            />
          ) : (
            ""
          )}
          {errormsg && errormsg.includes("document") ? (
            <Link to="/userdashboard">Click here to Add a document</Link>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }

  submitAddBooking() {
    const equipment = this.state.SelectedEquipments;
    const vehicle = this.state.selectedVehicle;
    const startDate = this.state.startDate;
    const endDate = this.state.returnDate;

    const payload = {
      startDate: startDate,
      endDate: endDate,
      userid: this.props.user,
      vehicleId: vehicle,
      bookingStatus: "Booking_Confirmed",
      equipmentId: equipment
    };
    let self = this;
    axios
      .post(`http://localhost:8080/api/booking/addBooking`, payload)
      .then(res => {
        console.log(res, "Res");
        if (res.status === 200) {
          message.success("Booking Confirmed!");
          this.setState({
            isConfirmed: true,
            isError: false
          });
        }
      })
      .catch(res => {
        if (res) {
          this.setState({
            isConfirmed: false,
            isError: true,
            ErrorMsg: res.response.data.message
          });
          // message.error(res.response.data.message);
        }
        console.log(res);
      });
  }
}

export default CarBooking;
