import React, { Component } from "react";
import "../Stylesheets/Checkout.css";
import { differenceInHours } from "date-fns";
import axios from "axios";

const Item = props => (
  <div className="item-container">
    <div className="item-image">
      <img src={props.img} />
      <div className="item-details">
        <h3 className="item-name"> {props.name} </h3>
        <h2 className="item-price"> {props.price} </h2>
      </div>
    </div>
  </div>
);

const Input = props => (
  <div className="input">
    <label>{props.label}</label>
    <div className="input-field">
      <input type={props.type} name={props.name} />
      <img src={props.imgSrc} />
    </div>
  </div>
);

const Button = props => (
  <button className="checkout-btn" type="button">
    {props.text}
  </button>
);

const Checkouts = props => (
  <div className="checkout">
    <div className="checkout-container">
      <h3 className="heading-3">Credit card checkout</h3>
      <Input label="Cardholder's Name" type="text" name="name" />
      <Input
        label="Card Number"
        type="number"
        name="card_number"
        imgSrc="https://seeklogo.com/images/V/visa-logo-6F4057663D-seeklogo.com.png"
      />
      <div className="row">
        <div className="col">
          <Input label="Expiration Date" type="month" name="exp_date" />
        </div>
        <div className="col">
          <Input label="CVV" type="number" name="cvv" />
        </div>
      </div>
    </div>
  </div>
);
class Checkout extends Component {
  state = {};

  componentDidMount() {
    const token = localStorage.getItem("JWTToken");
    this.setState({
      token: token
    });
    // this.getEquipmentDetails(token);
    // this.getVehicleById(token);
    const propState = this.props.location.state;
    this.getPaymentDetails(token, propState.bookingId);
  }

  render() {
    //const bookingID = propState.bookingID;
    return (
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "center",
          height: "100vh",
          alignItems: "center"
        }}
      >
        <div className="app-container">
          <div className="row">
            <div className="col">
              <Item
                name="Instax Mini 90 Neo Classic"
                price={"LKR "}
                img="http://ecx.images-amazon.com/images/I/61%2BABMMN5zL._SL1500_.jpg"
              />
            </div>
            <div className="col no-gutters">
              <Checkouts />
              {/* <Button
                onClick={e => {
                  this.updateStatus(propState.bookingID, "Completed");
                }}
                text="Complete Booking"
              /> */}
            </div>
          </div>
        </div>
      </div>
    );
  }

  calculateTotalAmount() {
    const propstate = this.props.location.state;
    let totalamount = 0;
    if (propstate && this.state) {
      let rate = this.state.vehicle.vehicleRate;
      var diff = differenceInHours(propstate.returnDate, propstate.startDate);
      let total = rate * diff;
      var equipmentrates = this.state.equipmentsData.map(
        equipment => equipment.costPerHour
      );
      let eqsum = 0;
      let tot = 0;
      if (equipmentrates.length > 0) {
        eqsum = equipmentrates.reduce((result, number) => result + number);
        tot = eqsum * diff;
      }

      totalamount = total + tot;
    }
    return totalamount;
  }

  getVehicleById = token => {
    const propstate = this.props.location.state;
    let id = propstate.vehicleId;
    let self = this;
    axios
      .get(`http://localhost:8080/api/vehicle/getvehicle/${id}`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          console.log(res.data);
          self.setState({
            vehicle: res.data
          });
        }
      });
  };

  getEquipmentDetails(token) {
    const propstate = this.props.location.state;
    let ids = propstate ? propstate.equipmentId : null;
    let equipments = [];
    if (ids && ids.length > 0) {
      ids.map(id => {
        axios
          .get(`http://localhost:8080/api/equipments/getbyid/${id}`, {
            headers: {
              Authorization: "Bearer " + token
            }
          })
          .then(res => {
            if (res.status === 200) {
              equipments.push(res.data);
            }
          });
      });

      let equipmentNames = equipments.map(equipment => equipment.equipmentName);
      console.log(equipmentNames);
      this.setState({
        equipmentsData: equipments
      });
    }
  }

  getPaymentDetails = (token, bookingid) => {
    // const propstate = this.props.location.state;
    // const bookingid = propstate.bookingID;

    axios
      .get(`http://localhost:8080/api/booking/calcPayment/${bookingid}`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          this.setState({
            paymentData: res.data
          });
          console.log(res.data);
        }
      });
  };

  updateStatus = (id, bkStatus) => {
    // console.log(id, bkStatus);
    const payload = {
      bookingStatus: bkStatus
    };
    //let status = bkStatus;
    axios
      .put(`http://localhost:8080/api/booking/updateStatus/${id}`, payload, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          console.log("updated");
          this.componentDidMount();
        }
      });
  };
}

export default Checkout;
