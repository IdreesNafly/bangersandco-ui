import React, { Component } from "react";
import "../Stylesheets/CheckoutPg.css";
import {
  Divider,
  Badge,
  Descriptions,
  Button,
  Drawer,
  Modal,
  notification
} from "antd";
import axios from "axios";
import ReactToPrint, { PrintContextConsumer } from "react-to-print";
import Pdf from "react-to-pdf";
import { Link } from "react-router-dom";
import "../Stylesheets/Checkout.css";
import cogoToast from "cogo-toast";
import {
  CloudDownloadOutlined,
  ExclamationCircleOutlined
} from "@ant-design/icons";
const ref = React.createRef();
const { confirm } = Modal;

class CheckoutPage extends Component {
  state = { visible: false };

  componentDidMount() {
    const token = localStorage.getItem("JWTToken");

    // this.getEquipmentDetails(token);
    // this.getVehicleById(token);
    const propState = this.props.location.state;
    this.setState({
      token: token,
      bkId: propState.bookingId
    });
    this.getPaymentDetails(token, propState.bookingId);
  }

  render() {
    const data = this.state.paymentData;
    return (
      <div className="MainWrapper">
        {/* <Pdf targetRef={ref} filename="code-example.pdf">
          {({ toPdf }) => <button onClick={toPdf}>Generate Pdf</button>}
        </Pdf> */}
        <div className="Wrapper" ref={ref}>
          <div className="header">
            <div
              style={{
                fontWeight: "bold",
                color: "darkblue",
                fontSize: "1.5rem"
              }}
            >
              Banger and Co
            </div>
            <div>No: 10 Galle Road, Colombo 03</div>
            <div>Phone No: 077723584</div>
            <div>Email: info@banger.com</div>
            <div
              style={{
                fontVariantCaps: "all-small-caps",
                fontStyle: "italic",
                fonWeight: 500,
                color: "blue"
              }}
            >
              Your Premium Car rental Company
            </div>
          </div>
          <Divider />
          {data ? (
            <div>
              <div>{data.username}</div>
              <div>{data.useremail}</div>
            </div>
          ) : (
            ""
          )}

          <div style={{ display: "flex", flex: 1, flexDirection: "column" }}>
            {data ? (
              <Descriptions title="Invoice" layout="vertical" bordered>
                <Descriptions.Item label="Booking Date">
                  {data.startDate}
                </Descriptions.Item>
                <Descriptions.Item label="Return Date">
                  {data.returnDate}
                </Descriptions.Item>
                <Descriptions.Item label="Extended Date">
                  {data.extendedDate}
                </Descriptions.Item>
                <Descriptions.Item label="Vehicle Model">
                  {data.vehicleModel}
                </Descriptions.Item>
                <Descriptions.Item label="Vehicle Type" span={2}>
                  {data.vehicleType}
                </Descriptions.Item>
                {/* <Descriptions.Item label="Status" span={3}>
                <Badge status="processing" text="Running" />
              </Descriptions.Item> */}
                <Descriptions.Item label="Vehice Rate">
                  {"LKR: " + data.totalVehicleRate}
                </Descriptions.Item>
                <Descriptions.Item label="Equipment Rate">
                  {"LKR: " + data.totalEquipmentRate}
                </Descriptions.Item>
                <Descriptions.Item label="Total Amount">
                  {"LKR: " + data.totalamount}
                </Descriptions.Item>
                <Descriptions.Item label="Booking Summary">
                  {"Total Hours Booked: " + data.totalBookingHours}
                  <br />
                  {"Total Extended hours: " + data.extendedHours}
                  <br />
                  {"Vehicle Rate Per hour: " + data.vehiclerate}
                  <br />
                  {"Equipment Rate Per hour: " + data.equipmentrate}
                  <br />
                  {"No Of Equipements: " + data.noOfEquipments}
                  <br />
                </Descriptions.Item>
              </Descriptions>
            ) : (
              ""
            )}
          </div>
          <div
            style={{
              fontStyle: "italic",
              fontWeight: 400,
              alignSelf: "center",
              marginTop: "1rem"
            }}
          >
            Thank you for choosing us for your vehicle Rentals
          </div>
        </div>
        <div className="SummaryBox">
          <span className="checkoutHeader">Checkout</span>

          <div style={{ marginTop: "2rem" }}>{"Grand Total: "}</div>
          {data ? (
            <div className="fontTotal">{"LKR " + data.totalamount}</div>
          ) : (
            ""
          )}
          {/* <Link>
            <CloudDownloadOutlined />
            Print this Document
          </Link> */}
          <Pdf targetRef={ref} filename="BookingInvoice.pdf">
            {({ toPdf }) => (
              <Button style={{ marginBottom: "0.5rem" }} onClick={toPdf}>
                <CloudDownloadOutlined />
                Print Document
              </Button>
            )}
          </Pdf>
          <Button
            className="btn0"
            onClick={e => showConfirm(this.state.bkId, this.state.token)}
          >
            Pay By Cash
          </Button>
          <Button className="btn1" onClick={this.showDrawer}>
            Pay By Card
          </Button>
        </div>
        <Drawer
          title="Enter Your Card Details"
          placement="right"
          closable={false}
          onClose={this.onClose}
          visible={this.state.visible}
          width={500}
        >
          <div className="col no-gutters">
            <div className="checkout">
              <div className="checkout-container">
                <h3 className="heading-3">Credit card checkout</h3>
                <Input label="Cardholder's Name" type="text" name="name" />
                <Input
                  label="Card Number"
                  type="number"
                  name="card_number"
                  imgSrc="https://seeklogo.com/images/V/visa-logo-6F4057663D-seeklogo.com.png"
                />
                <div className="row">
                  <div className="col">
                    <Input
                      label="Expiration Date"
                      type="month"
                      name="exp_date"
                    />
                  </div>
                  <div className="col">
                    <Input label="CVV" type="number" name="cvv" />
                  </div>
                </div>
                <ChkButton
                  text="Confirm Payment"
                  id={this.state.bkId}
                  token={this.state.token}
                ></ChkButton>
              </div>
            </div>
          </div>
        </Drawer>
      </div>
    );
  }

  showDrawer = () => {
    this.setState({
      visible: true
    });
  };

  onClose = () => {
    this.setState({
      visible: false
    });
  };

  getPaymentDetails = (token, bookingid) => {
    // const propstate = this.props.location.state;
    // const bookingid = propstate.bookingID;

    axios
      .get(`http://localhost:8080/api/booking/calcPayment/${bookingid}`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          this.setState({
            paymentData: res.data
          });
          console.log(res.data);
        }
      });
  };
}
function showConfirm(id, token) {
  confirm({
    title: "Confirm Payment by Cash",
    icon: <ExclamationCircleOutlined />,
    content: "",
    onOk() {
      makePayment(id, token);
    },
    onCancel() {
      console.log("Cancel");
    }
  });
}

const Item = props => (
  <div className="item-container">
    <div className="item-image">
      <img src={props.img} />
      <div className="item-details">
        <h3 className="item-name"> {props.name} </h3>
        <h2 className="item-price"> {props.price} </h2>
      </div>
    </div>
  </div>
);

const Input = props => (
  <div className="input">
    <label>{props.label}</label>
    <div className="input-field">
      <input type={props.type} name={props.name} />
      <img src={props.imgSrc} />
    </div>
  </div>
);

const ChkButton = props => (
  <button
    onClick={e => makePayment(props.id, props.token)}
    className="checkout-btn"
    type="button"
  >
    {props.text}
  </button>
);

function makePayment(id, token) {
  cogoToast.success("Payment Made Successfully");
  updateStatus(id, token);
  window.location.href = "/userdashboard";
}

function updateStatus(id, token) {
  // console.log(id, bkStatus);
  const payload = {
    bookingStatus: "Completed"
  };
  //let status = bkStatus;
  axios
    .put(`http://localhost:8080/api/booking/updateStatus/${id}`, payload, {
      headers: {
        Authorization: "Bearer " + token
      }
    })
    .then(res => {
      if (res.status === 200) {
        console.log("updated");
      }
    });
}
export default CheckoutPage;
