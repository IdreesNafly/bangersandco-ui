import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

const FooterPage = () => {
  return (
    <MDBFooter
      style={{ background: "#313740" }}
      className="font-small pt-4 mt-4"
    >
      <MDBContainer fluid className="text-center text-md-left">
        <MDBRow>
          <MDBCol md="6">
            <h5 className="title">{"Bangers&Co"}</h5>
            <p>Travel hassle-free.</p>
          </MDBCol>
          <MDBCol md="6">
            <h5 className="title"></h5>
            <ul>
              <li className="list-unstyled">
                <a href="/">Home</a>
              </li>
              <li className="list-unstyled">
                <a href="/Fleet">Our Fleet</a>
              </li>
              <li className="list-unstyled">
                <a href="/About">About us</a>
              </li>
              <li className="list-unstyled">
                <a href="/Contact">Contact Us</a>
              </li>
            </ul>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <div className="footer-copyright text-center py-3">
        <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright:{" "}
          <a href="/Home"> {"Bangers&Co"} </a>
        </MDBContainer>
      </div>
    </MDBFooter>
  );
};

export default FooterPage;
