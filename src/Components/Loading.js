import React from "react";
import { Spin } from "antd";

const Loading = props => {
  return (
    <div>
      <Spin size="large" />
      <div>{"Loading..."}</div>
    </div>
  );
};

export default Loading;
