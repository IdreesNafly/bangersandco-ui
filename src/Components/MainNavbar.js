import React, { Component } from "react";
import { BrowserRouter as Router, withRouter } from "react-router-dom";
import { Avatar, Icon, Menu, Dropdown } from "antd";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavbarToggler,
  MDBCollapse,
  MDBContainer
} from "mdbreact";
import styled from "styled-components";
import { AuthConsumer } from "../AuthContext";
import { ROLE } from "../Utils";

const StyledLink = styled.a`
  color: black;
  font-weight: bold;
  margin-right: 1rem;
`;

const menu = (
  <Menu onClick={handleMenuClick}>
    <Menu.Item key="DASHBOARD">
      <Icon type="user" />
      My Dashboard
    </Menu.Item>
    <Menu.Item key="LOGOUT">
      <Icon type="user" />
      Logout
    </Menu.Item>
  </Menu>
);

function handleMenuClick(e) {
  if (e.key === "DASHBOARD") {
    window.location.href = "/userdashboard";
  } else {
    localStorage.clear();
    window.location.href = "/";
  }
}

class MainNavbar extends Component {
  state = {
    collapsed: false
  };

  handleTogglerClick = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  componentDidMount() {}
  render() {
    const navStyle = {
      marginBottom: "",
      backgroundColor: "white"
      //position: "relative"
    };

    console.log(ROLE, "role");
    const overlay = (
      <div
        id="sidenav-overlay"
        style={{ backgroundColor: "transparent" }}
        onClick={this.handleTogglerClick}
      />
    );
    console.log(window.location.href);
    if (
      window.location.href.includes("checkout") ||
      window.location.href.includes("admin")
    ) {
      return "";
    } else
      return (
        // <div style={{ height: "4rem" }}>
        /* <AuthConsumer> */
        /* {value => <div>{console.log(value)}</div>} */

        <MDBNavbar
          color="#343a40 !important"
          style={navStyle}
          dark
          expand="md"
          fixed="top"
          scrolling
          transparent
        >
          <AuthConsumer>
            {({ isAuth, logout, email }) => (
              <MDBContainer>
                <MDBNavbarBrand>
                  <strong className="black-text">{"Bangers&Co"}</strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler onClick={this.handleTogglerClick} />
                <MDBCollapse isOpen={this.state.collapsed} navbar>
                  <MDBNavbarNav left>
                    <MDBNavItem active>
                      <StyledLink href="/">Home</StyledLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <StyledLink href="/Fleet">Our Fleet</StyledLink>
                    </MDBNavItem>
                    {isAuth ? (
                      <MDBNavItem>
                        <StyledLink href="/Rental">Rentals</StyledLink>
                      </MDBNavItem>
                    ) : (
                      ""
                    )}
                    <MDBNavItem>
                      <StyledLink href="/About">About Us</StyledLink>
                    </MDBNavItem>
                    <MDBNavItem>
                      <StyledLink href="/Contact">Contact Us</StyledLink>
                    </MDBNavItem>
                  </MDBNavbarNav>
                  <MDBNavbarNav right>
                    {(isAuth && email) ||
                    (this.state.token && this.state.email) ? (
                      <Dropdown.Button
                        overlay={menu}
                        icon={<Icon type="user" />}
                      >
                        {email}
                      </Dropdown.Button>
                    ) : (
                      <div style={{ display: "flex", flexDirection: "row" }}>
                        <MDBNavItem>
                          <StyledLink href="/Signin">Sign In</StyledLink>
                        </MDBNavItem>
                        <MDBNavItem>
                          <StyledLink href="/Signup">Sign Up</StyledLink>
                        </MDBNavItem>
                      </div>
                    )}
                  </MDBNavbarNav>
                </MDBCollapse>
              </MDBContainer>
            )}
          </AuthConsumer>
          {this.state.collapsed && overlay}
        </MDBNavbar>
        /* </AuthConsumer> */
        // </div>
      );
  }
}

export default withRouter(MainNavbar);
