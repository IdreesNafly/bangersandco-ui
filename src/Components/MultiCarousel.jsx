// import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import React, { Component } from "react";
// import { withStyles } from "@material-ui/core/styles";
import axios from "axios";
import { message, Card, Carousel } from "antd";
import Loading from "./Loading";
import cogoToast from "cogo-toast";
const { Meta } = Card;

const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
};
function onChange(a, b, c) {
  console.log(a, b, c);
}

class MultiCarousel extends Component {
  state = {
    loading: true,
    error: false
  };

  componentDidMount() {
    this.getWebScrapingData();
    this.getvehicleData();
  }
  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          padding: "3rem"
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            fontSize: "2rem",
            fontWeight: "bold",
            marginBottom: "2rem",
            marginTop: "2rem"
          }}
        >
          Vehicle Price Comparison
        </div>
        <div style={{ display: "flex", flex: 1 }}>
          {this.renderWebDataView()}
        </div>
        <div style={{ display: "flex", flex: 1, marginTop: "1rem" }}>
          {this.renderVehicleDataView()}
        </div>
      </div>
    );
  }

  getWebScrapingData() {
    axios
      .get("http://localhost:8080/api/webscrape/fetch")
      .then(res => {
        if (res.status === 200) {
          this.setState({
            webData: res.data,
            loading: false,
            error: false
          });
        }
      })
      .catch(err => {
        this.setState({
          loading: false,
          error: true
        });
        cogoToast.error("Error scraping Data!");
      });
  }

  getvehicleData() {
    axios
      .get(`http://localhost:8080/api/vehicle/allvehicle`)
      .then(res => {
        const vehicledata = res.data;
        this.setState({
          vehicleData: vehicledata,
          isLoading: false,
          error: false
        });
      })
      .catch(error => {
        this.setState({
          isLoading: false,
          error: true
        });
      });
  }

  renderWebDataView() {
    let data = this.state.webData;
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            flex: 1,
            justifyContent: "Center",
            alignItems: "center"
          }}
        >
          <Loading />
        </div>
      );
    }
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center"
        }}
      >
        {data && data.length > 0 ? (
          data.map(datum => {
            return (
              <Card
                hoverable
                style={{ width: 265, height: 270, marginRight: "1rem" }}
                cover={<img alt="example" src={datum.url} />}
              >
                <Meta
                  title={datum.name}
                  description={datum.price + " Per Day"}
                />
              </Card>
            );
          })
        ) : (
          <div
            style={{
              display: "flex",
              flex: 1,
              justifyContent: "Center",
              alignItems: "center"
            }}
          >
            Could Not Load Data!
          </div>
        )}
      </div>
    );
  }
  renderVehicleDataView() {
    let data = this.state.vehicleData;
    if (this.state.loading) {
      return (
        <div
          style={{
            display: "flex",
            flex: 1,
            justifyContent: "Center",
            alignItems: "center"
          }}
        >
          <Loading />
        </div>
      );
    }
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center"
        }}
      >
        {data && data.length > 0 ? (
          data.map(datum => {
            return (
              <Card
                hoverable
                style={{ width: 265, height: 270, marginRight: "1rem" }}
                cover={<img alt="example" src={datum.imagePath} />}
              >
                <Meta
                  title={datum.vehicleType}
                  description={datum.vehicleRate + " Per Day"}
                />
              </Card>
            );
          })
        ) : (
          <div
            style={{
              display: "flex",
              flex: 1,
              justifyContent: "Center",
              alignItems: "center"
            }}
          >
            Could Not Load Data!
          </div>
        )}
      </div>
    );
  }
}

export default MultiCarousel;
