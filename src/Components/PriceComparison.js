import React, { Component } from "react";
import styled from "styled-components";
import axios from "axios";
import cogoToast from "cogo-toast";
import Loading from "./Loading";
import { Divider } from "antd";

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin: ${props => (props.isUser ? "0 5rem 0 5rem" : "0")};
`;

const Header = styled.div`
  display: flex;
  flex: 1;
  background: #82b1ff;
  flex-direction: row;
`;

const HeaderItem = styled.div`
  display: flex;
  flex: 1;
  font-size: 1.3rem;
  font-weight: 400;
  justify-content: center;
`;

const HeaderSpan = styled.span`
  display: flex;
  justify-content: center;
  font-size: 1.5rem;
  font-weight: 600;
`;

const Body = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;
const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1;
  border: 1px solid;
  color: white;
`;
const RowItem = styled.div`
  display: flex;
  flex: 1;
  background: #3f51b5;
  padding: 0.6rem;
  margin: 1px;
  justify-content: center;
  align-items: center;
`;
class PriceComparison extends Component {
  state = {
    loading: true,
    error: false
  };

  componentDidMount() {
    this.getWebScrapingData();
    this.getvehicleData();
  }
  render() {
    const webData = this.state.transformedData;
    console.log(this.props);

    const isUser = this.props.isUser;
    return (
      <Wrapper isUser={isUser}>
        {isUser ? (
          <span
            style={{
              fontWeight: "bold",
              display: "flex",
              justifyContent: "center",
              fontSize: "2.5rem",
              marginBottom: "2rem",
              color: isUser ? "" : "#FFF"
            }}
          >
            Compare our prices
          </span>
        ) : (
          ""
        )}

        <Header>
          <HeaderItem
            style={{
              justifyContent: "center",
              alignItems: "center",
              fontWeight: 600
            }}
          >
            Vehicle Type
          </HeaderItem>
          <HeaderItem style={{ flexDirection: "column" }}>
            <HeaderSpan>Their prices</HeaderSpan>
            <div style={{ display: "flex", flexDirection: "row", flex: 1 }}>
              <HeaderItem>Per Day</HeaderItem>
              <HeaderItem>Per Week</HeaderItem>
              <HeaderItem>Per Month</HeaderItem>
            </div>
          </HeaderItem>
          <HeaderItem style={{ flexDirection: "column" }}>
            <HeaderSpan>Our prices</HeaderSpan>
            <div style={{ display: "flex", flexDirection: "row", flex: 1 }}>
              <HeaderItem>Per Day</HeaderItem>
              <HeaderItem>Per Week</HeaderItem>
              <HeaderItem>Per Month</HeaderItem>
            </div>
          </HeaderItem>
        </Header>
        {webData && webData.length > 0 ? (
          <Body>
            {webData.map(data => {
              return (
                <Row>
                  <RowItem
                    style={{
                      fontWeight: "bold"
                    }}
                  >
                    {data.name}
                  </RowItem>
                  <RowItem>
                    <RowItem>
                      {data.pricePerDay ? "LKR " + data.pricePerDay : "N/A"}
                    </RowItem>
                    <RowItem>
                      {data.pricePerWeek ? "LKR " + data.pricePerWeek : "N/A"}
                    </RowItem>
                    <RowItem>
                      {data.pricePerMonth ? "LKR " + data.pricePerMonth : "N/A"}
                    </RowItem>
                  </RowItem>

                  <RowItem>
                    <RowItem>
                      {data.ourPricePerDay
                        ? "LKR " + data.ourPricePerDay
                        : "N/A"}
                    </RowItem>
                    <RowItem>
                      {data.ourPricePerWeek
                        ? "LKR " + data.ourPricePerWeek
                        : "N/A"}
                    </RowItem>
                    <RowItem>
                      {data.ourPricePerMonth
                        ? "LKR " + data.ourPricePerMonth
                        : "N/A"}
                    </RowItem>
                  </RowItem>
                </Row>
              );
            })}
          </Body>
        ) : this.state.loading ? (
          <div
            style={{
              display: "flex",
              flex: 1,
              justifyContent: "center",
              marginTop: "3rem"
            }}
          >
            <Loading />
          </div>
        ) : (
          <div>No Data to Display</div>
        )}
      </Wrapper>
    );
  }

  getWebScrapingData() {
    axios
      .get("http://localhost:8080/api/webscrape/fetch")
      .then(res => {
        if (res.status === 200) {
          this.transformData(res.data);
          this.setState({
            webData: res.data,
            loading: false,
            error: false
          });
        }
      })
      .catch(err => {
        this.setState({
          loading: false,
          error: true
        });
        cogoToast.error("Error scraping Data!");
      });
  }

  transformData(data) {
    let arr = [];
    // data.map(datum => {
    // //   let price = datum.price;
    // //   var res = price.substring(7, 12);
    // //   let priceNum = Number(res);
    // //   let priceInLKR = priceNum * 75;
    // //   let pricePerHour = priceInLKR / 24;
    // //   let pricePerWeek = priceInLKR * 7;
    //   // let obj = {
    //   //   // imgurl: datum.url,
    //   //   type: datum.name,
    //   //   pricePerDay: Math.round(datum.pricePerDay),
    //   //   pricePer: Math.round(datum.pricePerWeek),
    //   //   pricePerWeek: Math.round(datum.pricePerMonth)
    //   // };
    //   arr.push(obj);
    // });

    let towncarPrice;
    let familysaloonprice;
    let familyestateprice;
    let mediumvanprice;
    let hatchbakprice;
    this.state.vehicleData &&
      this.state.vehicleData.map(dataveh => {
        if (dataveh.vehicleType === "TOWNCAR") {
          towncarPrice = dataveh.vehicleRate;
        } else if (dataveh.vehicleType === "SALOON") {
          familysaloonprice = dataveh.vehicleRate;
        } else if (dataveh.vehicleType === "VAN") {
          mediumvanprice = dataveh.vehicleRate;
        } else if (dataveh.vehicleType === "ESTATE") {
          familyestateprice = dataveh.vehicleRate;
        } else if (dataveh.vehicleType === "HATCHBACK") {
          hatchbakprice = dataveh.vehicleRate;
        }
      });

    console.log(
      towncarPrice,
      familysaloonprice,
      mediumvanprice,
      familyestateprice,
      hatchbakprice
    );

    data[0].ourPricePerDay = towncarPrice;
    data[0].ourPricePerWeek = towncarPrice * 7;
    data[0].ourPricePerMonth = towncarPrice * 30;
    data[0].ourType = "Town Car";

    data[1].ourPricePerDay = hatchbakprice;
    data[1].ourPricePerWeek = hatchbakprice * 7;
    data[1].ourPricePerMonth = hatchbakprice * 30;
    data[1].ourType = "Hatchback";

    data[3].ourPricePerDay = familyestateprice;
    data[3].ourPricePerWeek = familyestateprice * 7;
    data[3].ourPricePerMonth = familyestateprice * 30;
    data[3].ourType = "Estate";

    data[4].ourPricePerDay = mediumvanprice;
    data[4].ourPricePerWeek = mediumvanprice * 7;
    data[4].ourPricePerMonth = mediumvanprice * 30;
    data[4].ourType = "Van";

    data[2].ourPricePerDay = familysaloonprice;
    data[2].ourPricePerWeek = familysaloonprice * 7;
    data[2].ourPricePerMonth = familysaloonprice * 30;
    data[2].ourType = "Saloon";
    this.setState({
      transformedData: data
    });
  }

  getvehicleData() {
    axios
      .get(`http://localhost:8080/api/vehicle/allvehicle`)
      .then(res => {
        const vehicledata = res.data;
        this.setState({
          vehicleData: vehicledata,
          isLoading: false,
          error: false
        });
      })
      .catch(error => {
        this.setState({
          isLoading: false,
          error: true
        });
      });
  }
}

export default PriceComparison;
