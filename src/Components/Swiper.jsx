import React from "react";
import Swiper from "react-id-swiper";
import "swiper/css/swiper.css";
import img from "../Images/header-bg.jpg";
import img2 from "../Images/callaction-bg.jpg";
const FadeEffect = () => {
  const params = {
    spaceBetween: 30,
    effect: "fade",
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    loop: true,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    }
  };

  return (
    <Swiper {...params}>
      <div style={{ height: "50vh" }}>
        <img src={img} />
      </div>
      <div>
        <img src={img2} />
      </div>
      <div>Slide #3</div>
      <div>Slide #4</div>
      <div>Slide #5</div>
    </Swiper>
  );
};

export default FadeEffect;
