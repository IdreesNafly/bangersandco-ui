import { Table } from "antd";
import "antd/dist/antd.css";
import React, { Component } from "react";

class TableComponent extends Component {
  state = {
    date: new Date()
  };

  render() {
    const columns = this.props.columns;
    const data = this.props.data;
    const expanded = this.props.expandedRowRender;
    const keys = this.props.expandedRowkeys;
    return (
      <Table
        columns={columns}
        bordered={true}
        expandedRowRender={e => expanded(e)}
        dataSource={data}
      />
    );
  }
}
export default TableComponent;
