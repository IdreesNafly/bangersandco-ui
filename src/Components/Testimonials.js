import React from "react";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBTestimonial,
  MDBAvatar,
  MDBIcon
} from "mdbreact";

const TestimonialsPage = () => {
  return (
    <MDBContainer>
      <section className="team-section text-center my-5">
        <h2 className="h1-responsive font-weight-bold my-5">Testimonials</h2>
        <p className="dark-grey-text w-responsive mx-auto mb-5">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit, error
          amet numquam iure provident voluptate esse quasi, veritatis totam
          voluptas nostrum quisquam eum porro a pariatur veniam.
        </p>

        <MDBRow className="text-center">
          <MDBCol md="4" className="mb-md-0 mb-5">
            <div>
              <div className="mx-auto">
                <img
                  src="https://mdbootstrap.com/img/Photos/Avatars/img%20(1).jpg"
                  alt=""
                  className="rounded-circle z-depth-1 img-fluid"
                />
              </div>
              <h4 className="font-weight-bold dark-grey-text mt-4">
                Anna Deynah
              </h4>
              <h6 className="font-weight-bold blue-text my-3">
                Friendly and Clean Service
              </h6>
              <p className="font-weight-normal dark-grey-text">
                <MDBIcon className="fa fa-quote-left pr-2" icon="" />I think
                Bangerand co represents great value and I can recommend them and
                espessially Mr. Jagath to anyone.
              </p>
              <div className="orange-text">
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon far icon="star-half" />
              </div>
            </div>
          </MDBCol>
          <MDBCol md="4" className="mb-md-0 mb-5">
            <div>
              <div className="mx-auto">
                <img
                  src="https://mdbootstrap.com/img/Photos/Avatars/img%20(8).jpg"
                  alt=""
                  className="rounded-circle z-depth-1 img-fluid"
                />
              </div>
              <h4 className="font-weight-bold dark-grey-text mt-4">John Doe</h4>
              <h6 className="font-weight-bold blue-text my-3">
                Perfect Rent A Car company
              </h6>
              <p className="font-weight-normal dark-grey-text">
                <MDBIcon className="fa fa-quote-left pr-2" />
                he company upgraded our car to a Land Cruiser Prado and provided
                us with a friendly, gentle and helpful driver called Palitha who
                helped us tremendously when we travelled throughout Sri Lanka.
                He was a very genuine, kind man and an excellent driver;
              </p>
              <div className="orange-text">
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
              </div>
            </div>
          </MDBCol>
          <MDBCol md="4">
            <div>
              <div className="mx-auto">
                <img
                  src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg"
                  alt=""
                  className="rounded-circle z-depth-1 img-fluid"
                />
              </div>
              <h4 className="font-weight-bold dark-grey-text mt-4">
                Maria Kate
              </h4>
              <h6 className="font-weight-bold blue-text my-3">
                Exceptional Service!
              </h6>
              <p className="font-weight-normal dark-grey-text">
                <MDBIcon className="fa fa-quote-left pr-2" />
                We can strongly recommend you to use both Banger and Co for car
                rental in Sri Lanka and Mr. Jagath Chandra as your best choice
                as your driver.
              </p>
              <div className="orange-text">
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon icon="star" />
                <MDBIcon far icon="star" />
              </div>
            </div>
          </MDBCol>
        </MDBRow>
      </section>
    </MDBContainer>
  );
};

export default TestimonialsPage;
