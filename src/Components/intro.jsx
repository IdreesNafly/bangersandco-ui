import React, { Component } from "react";
import { BrowserRouter as Router, Link, Redirect } from "react-router-dom";
import {
  MDBMask,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBView,
  MDBContainer
} from "mdbreact";
import bgImg from "../Images/car3.jpg";
import MainNavbar from "../Components/MainNavbar";

class MinimalisticIntro extends Component {
  componentDidMount() {
    const email = localStorage.getItem("email");
    this.setState({
      email: email
    });
  }
  render() {
    return (
      <>
        <MDBView src={bgImg} fixed>
          <MDBMask className="rgba-white-light d-flex justify-content-center align-items-center">
            <MDBContainer>
              <MDBRow>
                <MDBCol md="12" className="mb-4 white-text text-center">
                  <h1 className="display-3 mb-0 pt-md-5 pt-5 white-text font-weight-bold">
                    Bangers{" "}
                    <span className="indigo-text font-weight-bold">&Co</span>
                  </h1>
                  <hr className="hr-light my-4" />
                  <h5 className="text-uppercase pt-md-5 pt-sm-2 pt-5 pb-md-5 pb-sm-3 pb-5 white-text font-weight-bold">
                    The Exotic Vehicle Rental Company
                  </h5>
                  <MDBBtn
                    className="white btn-light-blue"
                    size="lg"
                    href="/Fleet"
                  >
                    Explore Inventory
                  </MDBBtn>
                  <MDBBtn
                    className="white btn-indigo"
                    size="lg"
                    onClick={e => {
                      this.onClick();
                    }}
                  >
                    Rent-a-Car Now
                  </MDBBtn>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </MDBMask>
        </MDBView>
      </>
    );
  }

  onClick = () => {
    return this.state.email && this.state.email !== null
      ? (window.location.href = "/Rental")
      : (window.location.href = "/SignIn");
  };
}

export default MinimalisticIntro;
