import React, { Component } from "react";
import MainNavbar from "../Components/MainNavbar";
import { Card } from "antd";
import styled from "styled-components";
import vision from "../Images/vision-mission.jpg";

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  height: 100vh;
  flex-direction: column;
  margin: 0 5rem;
`;

const CardStyle = {
  marginTop: "5rem",
  height: "35%",
  backgroundImage:
    "url(https://wall.bestcarmag.com/sites/default/files/audi-wallpaper-hd-38572-7939232.png)",
  backgroundPosition: "center"
};
const CardStyle2 = {
  marginTop: "1rem",
  height: "65%",
  marginBottom: "1rem"
};

const imgStyle = {
  width: "100%",
  alignSelf: "center"
};
class About extends Component {
  state = {};
  render() {
    return (
      <Wrapper>
        <Card hoverable style={CardStyle}></Card>
        <Card hoverable style={CardStyle2}>
          <img style={imgStyle} src={vision} alt={vision} />
        </Card>
      </Wrapper>
    );
  }
}

export default About;
