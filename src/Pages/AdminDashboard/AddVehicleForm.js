import {
  Form,
  Icon,
  Input,
  Button,
  Checkbox,
  Radio,
  Upload,
  Select,
  InputNumber
} from "antd";
import React, { Component } from "react";
import { Card } from "antd";

const { Meta } = Card;
const { Option } = Select;

const vehicleTypes = {
  TOWNCAR: "Town Car",
  HATCHBACK: "Family Hatchback",
  SALOON: "Family Saloon",
  ESTATE: "Family Estate",
  VAN: "Van"
};

const vehicleTransmission = {
  AUTOMATIC: "Automatic",
  MANUAL: "Manual"
};

const fuelType = {
  HYBRID: "Hybrid",
  PETROL: "Petrol",
  DIESEL: "Diesel"
};
class AddVehicle extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.addVehicle(values);
      }
    });
  };

  normFile = e => {
    console.log("Upload event:", e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Card
          hoverable
          style={{ display: "flex", flex: 1 }}
          // cover={
          //   <img
          //     alt="example"
          //     src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
          //   />
          // }
        >
          <div className="ant-card-body" style={{ display: "flex", flex: 1 }}>
            <Form
              style={{ flex: 1, flexDirection: "column" }}
              onSubmit={this.handleSubmit}
              className="login-form"
            >
              <div style={{ display: "flex", flexDirection: "row", flex: 1 }}>
                <div
                  style={{
                    display: "flex",
                    flex: 1,
                    flexDirection: "column",
                    marginRight: "15rem"
                  }}
                >
                  <Form.Item label="Vehicle Make">
                    {getFieldDecorator("vehicleMake", {
                      rules: [
                        {
                          // required: this.state.checkNick,
                          message: "Please enter Vehicle Make"
                        }
                      ]
                    })(<Input placeholder="Please input Vehicle Make" />)}
                  </Form.Item>
                  <Form.Item label="Vehicle Model">
                    {getFieldDecorator("vehicleModel", {
                      rules: [
                        {
                          //required: this.state.checkNick,
                          message: "Please input Vehicle Model"
                        }
                      ]
                    })(<Input placeholder="Please input Vehicle Model" />)}
                  </Form.Item>
                  <Form.Item label="Vehicle Transmission">
                    {getFieldDecorator("transmission")(
                      <Radio.Group>
                        {Object.entries(vehicleTransmission).map(trans => {
                          return (
                            <Radio.Button value={trans[0]}>
                              {trans[1]}
                            </Radio.Button>
                          );
                        })}
                      </Radio.Group>
                    )}
                  </Form.Item>

                  <Form.Item label="Vehicle Mileage">
                    {getFieldDecorator("mileage", { initialValue: 0 })(
                      <InputNumber min={1} max={10000000} />
                    )}
                    <span className="ant-form-text"> Miles</span>
                  </Form.Item>
                </div>
                <div
                  style={{ display: "flex", flex: 1, flexDirection: "column" }}
                >
                  <Form.Item label="Vehicle type" hasFeedback>
                    {getFieldDecorator("vehicleType", {
                      //rules: [{ required: true, message: "Please select Vehicle Type!" }]
                    })(
                      <Select placeholder="Select the Vehicle type">
                        {Object.entries(vehicleTypes).map(entry => {
                          return <Option value={entry[0]}>{entry[1]}</Option>;
                        })}
                      </Select>
                    )}
                  </Form.Item>
                  <Form.Item label="Year of Manufacture ">
                    {getFieldDecorator("YOM", { initialValue: 2020 })(
                      <InputNumber min={2000} max={2022} />
                    )}
                  </Form.Item>
                  <Form.Item label="Fuel type">
                    {getFieldDecorator("fuelType")(
                      <Radio.Group>
                        {Object.entries(fuelType).map(fuel => {
                          return (
                            <Radio.Button value={fuel[0]}>
                              {fuel[1]}
                            </Radio.Button>
                          );
                        })}
                      </Radio.Group>
                    )}
                  </Form.Item>
                  <Form.Item label="Vehicle Image">
                    {getFieldDecorator("imagePath", {
                      rules: [
                        {
                          //required: this.state.checkNick,
                          message: "Please enter Vehicle Image Address"
                        }
                      ]
                    })(<Input placeholder="Please input Vehicle image URL" />)}
                  </Form.Item>
                </div>
              </div>

              <Form.Item label="Vehicle Description">
                {getFieldDecorator("vehicleDescription", {
                  rules: [
                    {
                      //required: this.state.checkNick,
                      message: "Please enter Vehicle Description"
                    }
                  ]
                })(<Input placeholder="Please input Vehicle Description" />)}
              </Form.Item>

              <Form.Item label="Vehicle Rate">
                {getFieldDecorator("vehicleRate", { initialValue: 0 })(
                  <InputNumber min={3000} max={15000} />
                )}
                <span className="ant-form-text"> Dollars Per Day</span>
              </Form.Item>
              {/* <Form.Item label="Upload image">
          {getFieldDecorator("imagePath", {
            valuePropName: "fileList",
            getValueFromEvent: this.normFile
          })(
            <Upload.Dragger name="files" action="/upload.do">
              <p className="ant-upload-drag-icon">
                <Icon type="inbox" />
              </p>
              <p className="ant-upload-text">
                Click or drag file to this area to upload
              </p>
              <p className="ant-upload-hint">
                Support for a single or bulk upload.
              </p>
            </Upload.Dragger>
          )}
        </Form.Item> */}
              <Button
                style={{
                  color: "white",
                  width: "100%",
                  background: "#001529",
                  fontWeight: "bold"
                }}
                htmlType="submit"
              >
                Add Vehicle
              </Button>
            </Form>
          </div>
        </Card>
      </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create({ name: "normal_login" })(
  AddVehicle
);

// ReactDOM.render(<WrappedNormalLoginForm />, mountNode);
export default WrappedNormalLoginForm;
