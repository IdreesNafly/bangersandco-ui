import React, { Component } from "react";
import AdminDashboard from "./AdminDashboard";
import axios from "axios";

class AdminContainer extends Component {
  state = {
    token: null
  };

  componentDidMount() {
    const token = localStorage.getItem("JWTToken");
    this.setState({
      token: token
    });
    // this.addEquipment(token);
    // this.getAllEquipment(token);
    // this.addVehicle(token);
  }
  render() {
    return <AdminDashboard token={this.state.token} />;
  }
}

export default AdminContainer;
