import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  withRouter
} from "react-router-dom";
import { AuthConsumer } from "../../AuthContext";
import {
  MDBMask,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBView,
  MDBContainer
} from "mdbreact";
import bgImg from "../../Images/car3.jpg";
import PriceComparison from "../../Components/PriceComparison.js";
// class AdminDashboard extends Component {
//   state = {};
//   render() {
//     return <div>hi</div>;
//   }
// }

// export default AdminDashboard;
import Intro from "../../Components/intro";
import { Layout, Menu, Breadcrumb, Icon, Button } from "antd";
import ManageBooking from "./ManageBookings";
import ManageUsers from "./ManageUsers";
import ManageVehicles from "./ManageVehicles";
import ManageEquipments from "./ManageEquipments";
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class AdminDashboard extends Component {
  state = {
    collapsed: false
  };

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };
  renderConfirmation = () => {
    return <div>{"Confirmation"}</div>;
  };

  renderConfirmation2 = () => {
    return <div>{"Confirmation scdss"}</div>;
  };

  renderContent = e => {
    let componentToRender = null;

    switch (e) {
      case 1:
        componentToRender = this.renderConfirmation();
        break;
      case 2:
        componentToRender = this.renderConfirmation2();
        break;
      case 3:
        componentToRender = this.renderConfirmation();
        break;
      default:
        componentToRender = this.renderConfirmation();
    }

    return componentToRender;
  };

  render() {
    return (
      <Router>
        <Layout style={{ minHeight: "95vh" }}>
          <Sider
            collapsible
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
          >
            <div className="logo" />
            <Menu
              theme="dark"
              defaultSelectedKeys={[this.state.key]}
              selectedKeys={[this.state.key]}
              mode="inline"
              onClick={this.handleClick}
            >
              <Menu.Item>
                <Link to="/admindashboard">
                  <Icon type="pie-chart" />
                  <span
                    style={{
                      fontSize: "1.5rem",
                      color: "cyan",
                      fontWeight: "bold",
                      fontVariantCaps: "unicase"
                    }}
                  >
                    {"Bangers&Co"}
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item>
                <Link to="/admindashboard">
                  <Icon type="pie-chart" />
                  <span
                    style={{
                      fontSize: "1rem",
                      fontWeight: "bold",
                      color: "white"
                    }}
                  >
                    Admin Panel
                  </span>
                </Link>
              </Menu.Item>
              <Menu.Item key="1">
                <Link to="/admindashboard/managebookings">
                  <Icon type="pie-chart" />
                  <span>Manage Bookings</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/admindashboard/manageusers">
                  <Icon type="pie-chart" />
                  <span>Manage Users</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="3">
                <Link to="/admindashboard/manageequipments">
                  <Icon type="pie-chart" />
                  <span>Manage Equipments</span>
                </Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link to="/admindashboard/managevehicles">
                  <Icon type="pie-chart" />
                  <span>Manage Vehicles</span>
                </Link>
              </Menu.Item>
            </Menu>
            <div
              style={{
                display: "flex",
                flex: 1,
                height: "30%",
                justifyContent: "center",
                alignItems: "flex-end"
              }}
            >
              <Button onClick={this.onSignout}>Sign Out</Button>
            </div>
          </Sider>
          <Layout>
            {/* <Header style={{ background: "#fff", padding: 0 }} /> */}
            <Content style={{ margin: "0" }}>
              <AuthConsumer>
                {({ isAuth, logout, email, token }) => (
                  <Switch>
                    <Route exact path={"/admindashboard"}>
                      <Home />
                    </Route>
                    <Route exact path={"/admindashboard/managebookings"}>
                      <ManageBooking token={token} />
                    </Route>
                    <Route exact path={"/admindashboard/manageusers"}>
                      <ManageUsers token={token} />
                    </Route>
                    <Route exact path={"/admindashboard/managevehicles"}>
                      <ManageVehicles token={token} />
                    </Route>
                    <Route exact path={"/admindashboard/manageequipments"}>
                      <ManageEquipments token={token} />
                    </Route>
                  </Switch>
                )}
              </AuthConsumer>
            </Content>
            {/* <Footer style={{ textAlign: "center" }}>
              {"Bangers&Co ©2020 Created by Idrees Nafly"}
            </Footer> */}
          </Layout>
        </Layout>
      </Router>
    );
  }
  onSignout = () => {
    localStorage.clear();
    window.location.href = "/";
  };
  handleClick = e => {
    const key = e.key;
    console.log(key);
    this.renderContent(key);
    this.setState({
      key: key.toString()
    });
  };
}

const Home = () => {
  return (
    <MDBView src={bgImg} fixed>
      <MDBMask className="rgba-white-light d-flex justify-content-center align-items-center">
        <MDBContainer>
          <MDBRow>
            <MDBCol md="12" className="white-text text-center">
              <span className="indigo-text font-weight-bold">
                Welcome, Admin!
              </span>
              <h1 className="display-3 crimson-text font-weight-bold">
                Bangers{" "}
                <span className="indigo-text font-weight-bold">&Co</span>
              </h1>
              <hr className="hr-light" />
              <h5 className="text-uppercase crimson-text mb-5 font-weight-bold">
                The Exotic Vehicle Rental Company
              </h5>
            </MDBCol>
          </MDBRow>
          <MDBRow>
            <PriceComparison isUser={false} />
          </MDBRow>
        </MDBContainer>
      </MDBMask>
    </MDBView>
  );
};
export default withRouter(AdminDashboard);
