import React, { Component } from "react";
import Table from "../../Components/Table";
import axios from "axios";
import { Button, message } from "antd";
import styled from "styled-components";
import cogoToast from "cogo-toast";

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;

const Title = styled.span`
  display: flex;
  flex: 1;
  justify-content: center;
  font-size: 2.5rem;
  font-variant: all-small-caps;
  font-weight: 400;
  background: #ffbf00;
}
`;
const STATUS = {
  INPROGRESS: "InProgress",
  COMPLETED: "Completed"
};

class ManageBooking extends Component {
  state = {
    bookingData: [],
    loading: true,
    failed: false,
    a: true,
    token: ""
  };

  componentDidMount() {
    const token = localStorage.getItem("JWTToken");
    this.setState({
      token: token
    });
    this.getBookingsData();
  }

  transformColumnData = () => {
    let data = this.state.bookingData;
    let columns = [];
    if (data && data.length > 0) {
      const columnNames = Object.keys(data[0]);
      let col = {};
      let columnname;
      columnNames.map(column => {
        columnname = column.toUpperCase();
        col = {
          title: columnname,
          dataIndex: column,
          key: column
        };

        columns.push(col);
      });
    }
    return columns;
  };

  expandedRowRender = e => {
    const bookingId = e.bookingID;
    console.log(e);
    return (
      <ButtonWrapper>
        {/* <Button onClick={this.updateStatus}>Update</Button> */}
        <Button
          onClick={e => {
            this.deleteBooking(bookingId);
          }}
        >
          Delete
        </Button>
        {e.bookingStatus === "InProgress" || e.bookingStatus === "Completed" ? (
          ""
        ) : (
          <Button
            onClick={e => {
              this.updateStatus(bookingId, STATUS.INPROGRESS);
            }}
          >
            In Progress
          </Button>
        )}
        {/* <Button
          onClick={e => {
            this.updateStatus(bookingId, STATUS.COMPLETED);
          }}
        >
          Completed
        </Button> */}
      </ButtonWrapper>
    );
  };

  expandedRowkeys = e => {
    console.log(e);
  };
  transformTableData = () => {
    let data = this.state.bookingData;
    let bookingArr = [];
    console.log(data);
    if (data && data.length > 0) {
      data.map(booking => {
        booking.key = booking.bookingID;
        bookingArr.push(booking);
      });
    }
    console.log(bookingArr);
    return data;
  };

  handleRowClick = e => {
    console.log("row clicked", e);
  };
  render() {
    // this.transformColumnData();
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          padding: "3rem"
        }}
      >
        <Title>All Bookings</Title>
        <Table
          columns={this.transformColumnData()}
          data={this.transformTableData()}
          expandedRowRender={this.expandedRowRender}
          expandedRowkeys={this.expandedRowkeys}
        />
      </div>
    );
  }

  deleteBooking = id => {
    console.log(id);
    const that = this;
    axios
      .delete(`http://localhost:8080/api/booking/deleteBooking/${id}`)
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Deleted");
          // this.forceUpdate();
          this.componentDidMount();
        }
      });
  };

  updateStatus = (id, bkStatus) => {
    // console.log(id, bkStatus);
    const payload = {
      bookingStatus: bkStatus
    };
    //let status = bkStatus;
    axios
      .put(`http://localhost:8080/api/booking/updateStatus/${id}`, payload, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success("Status Updated");
          this.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Erro Updating");
      });
  };

  getBookingsData = () => {
    let self = this;
    let data = [];
    axios.get(`http://localhost:8080/api/booking/getAllBooking`).then(res => {
      if (res.status === 200) {
        data = res.data;
        self.setState({ bookingData: data, loading: false, failed: false });
      } else {
        self.setState({
          loading: true,
          failed: true
        });
      }
    });
  };

  /*------------------------------------BOOKINGS AXIOS CALLS------------------------------------------*/

  //  deleteBookingById = id => {
  //   axios
  //     .delete(`http://localhost:8080/api/booking/deleteBooking/${id}`, {
  //       headers: {
  //         Authorization: "Bearer " + this.state.token
  //       }
  //     })
  //     .then(res => {});
  // };

  // updateStatus = id => {
  //   const payload = {
  //     // vehicleid:1,
  //     // vehicleRate:100
  //   };
  //   axios
  //     .put(`http://localhost:8080/api/booking/updateStatus/${id}`, payload, {
  //       headers: {
  //         Authorization: "Bearer " + this.state.token
  //       }
  //     })
  //     .then(res => {});
  // };

  // getAllBooking = () => {
  //   axios
  //     .get(`http://localhost:8080/api/booking/getAllBooking`, {
  //       headers: {
  //         Authorization: "Bearer " + this.state.token
  //       }
  //     })
  //     .then(res => {});
  // };

  getBookingById = id => {
    axios
      .get(`http://localhost:8080/api/booking/getbookingbyid/${id}`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {});
  };

  getBookingByUser = id => {
    axios
      .get(`http://localhost:8080/api/booking/getBooking/${id}`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {});
  };
}

export default ManageBooking;
