import React, { Component } from "react";
import { Button, Modal, message } from "antd";
import Table from "../../Components/Table";
import styled from "styled-components";
import axios from "axios";
import { Card } from "antd";

const { Meta } = Card;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;
const Title = styled.span`
  display: flex;
  flex: 1;
  justify-content: center;
  font-size: 2.5rem;
  font-variant: all-small-caps;
  font-weight: 400;
  background: #ffbf00;
`;
class ManageEquipments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      equipmentData: [],
      token: null,
      name: "",
      price: 0,
      modalVisibility: false,
      equipmentId: "",
      selName: "",
      selPrice: ""
    };
    this.addEquipment = this.addEquipment.bind(this);
    this.mySubmitHandler = this.muSubmitHandler.bind(this);
    this.nameChangeHandler = this.nameChangeHandler.bind(this);
    this.priceChangeHandler = this.priceChangeHandler.bind(this);
    this.updateHandler = this.updateHandler.bind(this);
  }

  showModal = equipment => {
    //this.componentDidMount();
    this.setState({
      visible: true,
      equipmentId: equipment.id,
      selName: equipment.name,
      selPrice: equipment.price
    });
  };

  handleOk = e => {
    const name = this.state.name;
    const price = parseFloat(this.state.price);
    const id = this.state.equipmentId;

    //console.log(id, name, price);

    this.updateEquipment(price, id);
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  componentDidMount() {
    const token = localStorage.getItem("JWTToken");
    this.setState({
      token: token
    });
    this.getAllEquipment(token);
  }
  transformColumnData = () => {
    let data = this.state.equipmentData;
    let columns = [];
    if (data && data.length > 0) {
      const columnNames = Object.keys(data[0]);

      let col = {};
      let columnname;
      columnNames.map(column => {
        columnname = column.toUpperCase();
        col = {
          title: columnname,
          dataIndex: column,
          key: column
        };

        columns.push(col);
      });
    }

    columns.length = 3;

    console.log(columns);
    return columns;
  };

  expandedRowRender = e => {
    const id = e.equipmentId;
    const equipment = {
      id: id,
      name: e.equipmentName,
      price: e.costPerHour
    };
    return (
      <ButtonWrapper>
        <Button
          onClick={e => {
            this.deleteEquipment(id, this.state.token);
          }}
        >
          Remove Equipment
        </Button>
        <Button
          onClick={e => {
            this.showModal(equipment);
          }}
        >
          Update Equipment
        </Button>
      </ButtonWrapper>
    );
  };

  updateHandler = e => {
    e.preventDefault();
    console.log("sss");
  };
  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          padding: "3rem"
        }}
      >
        <Title>All Equipments</Title>
        {this.viewAllEquipments()}
        {this.addEquipmentForm()}
        <div>
          <Modal
            title="Update Equipment"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <form onSubmit={this.updateHandler}>
              <p>Cost Per Hour</p>
              <input
                type="text"
                onChange={this.priceChangeHandler}
                //defaultValue={this.state.selPrice}
              />
            </form>
          </Modal>
        </div>
      </div>
    );
  }

  // handleSubmit = values => {

  //   this.addEquipment(payload);
  // };

  viewAllEquipments = () => {
    return (
      <div>
        <Table
          columns={this.transformColumnData()}
          data={this.state.equipmentData}
          expandedRowRender={this.expandedRowRender}
          expandedRowkeys={this.expandedRowkeys}
        />
      </div>
    );
  };

  muSubmitHandler = event => {
    event.preventDefault();
    const name = this.state.name;
    const price = this.state.price;

    const payload = {
      equipmentName: name,
      costPerHour: price
    };
    if (payload) {
      this.addEquipment(payload);
    }
  };
  nameChangeHandler = event => {
    this.setState({
      name: event.target.value
    });
  };
  priceChangeHandler = event => {
    this.setState({
      price: event.target.value
    });
  };
  addEquipmentForm = () => {
    return (
      <div>
        <Title>Add Equipments</Title>
        <Card
          hoverable
          style={{ display: "flex", flex: 1, height: "20rem" }}
          cover={
            <img
              alt="example"
              style={{ height: "100%" }}
              src="https://bizimages.withfloats.com/actual/5811b7f79ec66804e4486e73.jpg"
            />
          }
        >
          <form style={{ padding: "2rem" }} onSubmit={this.mySubmitHandler}>
            <p>Equipment Name</p>
            <input type="text" onChange={this.nameChangeHandler} />
            <p>Cost Per Hour</p>
            <input type="text" onChange={this.priceChangeHandler} />
            <input style={{ marginTop: "1rem" }} type="submit" />
          </form>
        </Card>
      </div>
    );
  };

  /*------------------------------------EQUIPMENT AXIOS CALLS------------------------------------------*/
  addEquipment = payload => {
    // const payload = {
    //   equipmentName: val.name,
    //   costPerHour: val.price
    // };

    axios
      .post(`http://localhost:8080/api/equipments/add`, payload, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          message.success("Equipment Added");
          this.componentDidMount();
        }
      });
  };

  deleteEquipment = (id, token) => {
    axios
      .delete(`http://localhost:8080/api/equipments/delete/${id}`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          message.success("Equipment Deleted!");
          this.componentDidMount();
        }
      });
  };

  updateEquipment = (price, id) => {
    const payload = {
      costPerHour: price
    };
    axios
      .put(`http://localhost:8080/api/equipments/update/${id}`, payload, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          message.success("Equipment Price Updated!");
          this.componentDidMount();
        }
      });
  };

  getAllEquipment = token => {
    axios
      .get(`http://localhost:8080/api/equipments/all`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        this.setState({
          equipmentData: res.data
        });
      });
  };
}

export default ManageEquipments;
