import React, { Component } from "react";
import Table from "../../Components/Table";
import styled from "styled-components";
import { Button } from "antd";
import axios from "axios";
import cogoToast from "cogo-toast";
const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;

const Title = styled.span`
  display: flex;
  flex: 1;
  justify-content: center;
  font-size: 2.5rem;
  font-variant: all-small-caps;
  font-weight: 400;
  background: #ffbf00;
`;

class ManageUsers extends Component {
  state = {
    userData: [],
    loading: true,
    failed: false,
    a: true
  };

  componentDidMount() {
    const token = localStorage.getItem("JWTToken");
    this.setState({
      token: token
    });
    this.getAllUser(token);
  }

  transformColumnData = () => {
    let data = this.state.userData;
    let columns = [];
    if (data && data.length > 0) {
      const columnNames = Object.keys(data[0]);
      let col = {};
      let columnname;
      columnNames.map(column => {
        columnname = column.toUpperCase();
        col = {
          title: columnname,
          dataIndex: column,
          key: column
        };

        columns.push(col);
      });
    }
    columns.length = 7;
    return columns;
  };

  expandedRowRender = e => {
    const userid = e.email;
    return (
      <div>
        {e.authentication.userRole === "Customer" ? (
          <ButtonWrapper>
            {/* <Button onClick={this.updateStatus}>Update</Button> */}
            <Button
              onClick={e => {
                this.deleteUserById(userid);
              }}
            >
              Delete User
            </Button>
            <Button
              onClick={e => {
                this.blacklistUser(userid, e.backlisted);
              }}
            >
              {e.backlisted ? "Unblacklist User" : "Blacklist User"}
            </Button>
            <Button>Message User</Button>
          </ButtonWrapper>
        ) : (
          <div style={{ display: "flex", justifyContent: "center" }}>
            This is an Admin User
          </div>
        )}
      </div>
    );
  };

  expandedRowkeys = e => {
    console.log(e);
  };
  transformTableData = () => {
    let data = this.state.bookingData;
    let bookingArr = [];
    console.log(data);
    if (data && data.length > 0) {
      data.map(booking => {
        booking.key = booking.bookingID;
        bookingArr.push(booking);
      });
    }
    console.log(bookingArr);
    return data;
  };

  handleRowClick = e => {
    console.log("row clicked", e);
  };
  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          padding: "3rem"
        }}
      >
        <Title>All Users</Title>
        <Table
          columns={this.transformColumnData()}
          data={this.state.userData}
          expandedRowRender={this.expandedRowRender}
          expandedRowkeys={this.expandedRowkeys}
        />
      </div>
    );
  }

  /*------------------------------------USERS AXIOS CALLS------------------------------------------*/

  addAdminUser = values => {
    const Authentication = {
      email: values.email,
      password: values.password,
      userRole: "Customer"
    };
    const User = {
      authentication: Authentication,
      email: values.email,
      firstName: values.firstName,
      lastName: values.lastName,
      gender: values.gender,
      contactNo: values.contactNo,
      address: values.address
    };
    axios
      .post(`http://localhost:8080/api/user/signup`, User, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {});
  };

  deleteUserById = id => {
    axios
      .delete(`http://localhost:8080/api/user/deleteuser/${id}`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          cogoToast.success(
            "User with ID: " + id + " was deleted Successfully!"
          );
          this.componentDidMount();
        }
      });
  };

  blacklistUser = (id, isBlacklisted) => {
    axios
      .put(`http://localhost:8080/api/user/blacklistuser/${id}`, null, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status == 200) {
          cogoToast.success("User Blacklisted");
          this.componentDidMount();
        }
      })
      .catch(err => {
        cogoToast.error("Error!");
      });
  };

  updateUser = val => {
    const payload = {
      // vehicleid:1,
      // vehicleRate:100
    };
    axios
      .put(`http://localhost:8080/api/user/updateuser/`, payload, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {});
  };

  getAllUser = token => {
    axios
      .get(`http://localhost:8080/api/user/getalluser`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        console.log(res.data);
        this.setState({
          userData: res.data
        });
      });
  };

  getUserById = id => {
    axios
      .get(`http://localhost:8080/api/user/getuser/${id}`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {});
  };
}

export default ManageUsers;
