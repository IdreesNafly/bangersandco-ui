import React, { Component } from "react";
import styled from "styled-components";
import Table from "../../Components/Table";
import { Button, Form, Input, Modal, Divider, message } from "antd";
import axios from "axios";
import AddVehicleForm from "./AddVehicleForm";
import {
  MDBBtn,
  MDBCard,
  MDBIcon,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText,
  MDBCol,
  MDBBtnGroup
} from "mdbreact";

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;
const Title = styled.span`
  display: flex;
  flex: 1;
  justify-content: center;
  font-size: 2.5rem;
  font-variant: all-small-caps;
  font-weight: 400;
  background: #ffbf00;
  margin-bottom: 1rem;
`;

class ManageVehicles extends Component {
  state = {
    vehicleData: [],
    token: "",
    vehicleID: ""
  };
  constructor(props) {
    super(props);
    this.addVehicle = this.addVehicle.bind(this);
    this.priceChangeHandler = this.priceChangeHandler.bind(this);
  }
  priceChangeHandler = event => {
    this.setState({
      price: event.target.value
    });
  };
  componentDidMount() {
    const token = localStorage.getItem("JWTToken");
    this.setState({
      token: token
    });
    this.getAllVehicle(token);
  }
  transformColumnData = () => {
    let data = this.state.vehicleData;
    let columns = [];
    if (data && data.length > 0) {
      const columnNames = Object.keys(data[0]);
      let col = {};
      let columnname;
      columnNames.map(column => {
        columnname = column.toUpperCase();
        col = {
          title: columnname,
          dataIndex: column,
          key: column
        };

        columns.push(col);
      });
    }
    return columns;
  };

  expandedRowRender = e => {
    const vehicleId = e.vehicleID;
    console.log(e);
    return (
      <ButtonWrapper>
        <Button
          onClick={e => {
            this.showModal(vehicleId);
          }}
        >
          Update vehicle rate
        </Button>
        <Button
          onClick={e => {
            this.deleteVehicleById(vehicleId);
          }}
        >
          Remove Vehicle
        </Button>
      </ButtonWrapper>
    );
  };

  expandedRowkeys = e => {
    console.log(e);
  };
  transformTableData = () => {
    let data = this.state.bookingData;
    let bookingArr = [];
    console.log(data);
    if (data && data.length > 0) {
      data.map(booking => {
        booking.key = booking.bookingID;
        bookingArr.push(booking);
      });
    }
    console.log(bookingArr);
    return data;
  };

  // handleRowClick = e => {
  //   console.log("row clicked", e);
  // };
  showModal = id => {
    //this.componentDidMount();
    this.setState({
      visible: true,
      vehicleID: id
    });
  };
  handleOk = e => {
    const price = parseFloat(this.state.price);
    const id = this.state.vehicleID;

    //console.log(id, name, price);

    this.updateVehicle(price, id);
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          padding: "3rem"
        }}
      >
        <Title>All Vehicles</Title>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            flex: 1,
            overflow: "auto"
          }}
        >
          {this.state.vehicleData &&
            this.state.vehicleData.map(vehicle => {
              return (
                // <Details>
                //   <Image src={vehicle.imagePath} alt={pic} />
                //   <Wrapper style={{ margin: "1%" }}>
                //     <Title>
                //       {vehicle.vehicleMake + " " + vehicle.vehicleModel}
                //     </Title>
                //     <Body>{vehicle.vehicleDescription}</Body>
                //     <TagsWrapper>
                //       <Item>{vehicle.mileage}</Item>
                //       <Item>{vehicle.fuelType}</Item>
                //       <Item>{vehicle.transmission}</Item>
                //       <Item>{vehicle.yom}</Item>
                //     </TagsWrapper>
                //   </Wrapper>
                // </Details>
                <MDBCol
                  style={{
                    maxWidth: "22rem",
                    minWidth: "20rem",
                    marginBottom: "1rem"
                  }}
                >
                  <MDBCard style={{ height: "530px" }}>
                    <MDBCardImage
                      className="img-fluid"
                      src={vehicle.imagePath}
                      waves
                    />

                    <MDBCardBody>
                      <MDBCardTitle>
                        {vehicle.vehicleMake + " " + vehicle.vehicleModel}
                      </MDBCardTitle>
                      <hr />
                      <MDBCardText>{vehicle.vehicleDescription}</MDBCardText>
                      <MDBCardText
                        style={{ color: "black", fontSize: "1.2rem" }}
                      >
                        {"LKR " + vehicle.vehicleRate + "  / Day"}
                      </MDBCardText>
                    </MDBCardBody>
                    <div
                      style={{ marginBottom: 0 }}
                      className="rounded-bottom mdb-color lighten-3 text-center pt-3"
                    >
                      <ul className="list-unstyled list-inline font-small">
                        <li className="list-inline-item pr-2 white-text">
                          <MDBIcon icon="gas-pump" /> {vehicle.fuelType}
                        </li>
                        <li className="list-inline-item pr-2">
                          <a href="#!" className="white-text">
                            <MDBIcon icon="cogs" className="mr-1" />
                            {vehicle.transmission}
                          </a>
                        </li>
                        <li className="list-inline-item pr-2">
                          <a href="#!" className="white-text">
                            <MDBIcon icon="taxi" className="mr-1" />
                            {vehicle.yom}
                          </a>
                        </li>
                        {/* <li className="list-inline-item">
                        <a href="#!" className="white-text">
                          <MDBIcon fab icon="twitter" className="mr-1" />5
                        </a>
                      </li> */}
                      </ul>
                      <Divider style={{ margin: 0 }} />
                      <ul
                        style={{ marginBottom: 0 }}
                        className="list-unstyled list-inline font-small"
                      >
                        <MDBBtnGroup size="sm">
                          <MDBBtn
                            onClick={e => {
                              this.deleteVehicleById(vehicle.vehicleID);
                            }}
                            color="#6c757d"
                          >
                            Delete
                          </MDBBtn>
                          <MDBBtn
                            onClick={e => {
                              this.showModal(vehicle.vehicleID);
                            }}
                            color="#6c757d"
                          >
                            Update Price
                          </MDBBtn>
                        </MDBBtnGroup>
                      </ul>
                    </div>
                  </MDBCard>
                </MDBCol>
              );
            })}
        </div>
        <br />
        <Title>Add Vehicle</Title>
        <AddVehicleForm addVehicle={this.addVehicle} />
        {/* <Table
          columns={this.transformColumnData()}
          data={this.state.vehicleData}
          expandedRowRender={this.expandedRowRender}
          expandedRowkeys={this.expandedRowkeys}
        />
        <div style={{ display: "flex", width: "60%" }}>
          <span>Add vehicle</span>
          <AddVehicleForm addVehicle={this.addVehicle} />
        </div>
        <div> */}
        <Modal
          title="Update Vehicle Rate"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <form onSubmit={this.updateHandler}>
            <p>Vehicle Rate</p>
            <input
              type="text"
              onChange={this.priceChangeHandler}
              //defaultValue={this.state.selPrice}
            />
          </form>
        </Modal>
      </div>
    );
  }

  /*------------------------------------VEHICLES AXIOS CALLS------------------------------------------*/
  addVehicle = payload => {
    // const payload = {
    //   vehicleType: "TOWN CAR",
    //   vehicleRate: 40,
    //   vehicleDescription:
    //     "The Kia Picanto is a city car produced by the South Korean manufacturer Kia Motors since 2004.",
    //   fuelType: "Petrol",
    //   transmission: "Automatic",
    //   mileage: "12500",
    //   imagePath:
    //     "https://http2.mlstatic.com/se-alquila-vehiculo-kia-picanto-2018-por-dia-D_NQ_NP_900448-MRD29580810905_032019-F.webp",
    //   vehicleMake: "Kia",
    //   vehicleModel: "Picanto",
    //   YOM: "2019"
    // };
    axios
      .post(`http://localhost:8080/api/vehicle/addVehicle`, payload, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          message.success("Vehicle Added");
          this.componentDidMount();
        }
      });
  };

  deleteVehicleById = id => {
    axios
      .delete(`http://localhost:8080/api/vehicle/deletevehicle/${id}`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          message.success("Vehicle Deleted");
          this.componentDidMount();
        }
      });
  };

  updateVehicle = (price, id) => {
    const payload = {
      vehicleid: id,
      vehicleRate: price
    };
    axios
      .put(`http://localhost:8080/api/vehicle/updatevehicle/`, payload, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        if (res.status === 200) {
          message.success("Vehicle Price Updated");
          this.componentDidMount();
        }
      });
  };

  getAllVehicle = token => {
    axios
      .get(`http://localhost:8080/api/vehicle/allvehicle`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        this.setState({
          vehicleData: res.data
        });
      });
  };

  getVehicleById = id => {
    axios
      .get(`http://localhost:8080/api/vehicle/getvehicle/${id}`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {});
  };
}
// const WrappedNormalLoginForm = Form.create({ name: "normal_login" })(
//   ManageVehicles
// );

export default ManageVehicles;
