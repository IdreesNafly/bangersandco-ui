import {
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBIcon,
  MDBBtn,
  MDBInput
} from "mdbreact";
import React, { Component } from "react";
import cogoToast from "cogo-toast";
import Axios from "axios";

const BASE_URL = "http://localhost:8080/";

class ContactPage extends Component {
  state = {
    email: "",
    subject: "",
    body: ""
  };
  handleBodyChange = e => {
    this.setState({
      body: e.target.value
    });
  };
  handleEmailChange = e => {
    this.setState({
      email: e.target.value
    });
  };
  handleSubjectChange = e => {
    this.setState({
      subject: e.target.value
    });
  };
  render() {
    return (
      <section style={{ padding: "3rem" }} className="my-5">
        <h2
          style={{
            display: "flex",
            flex: 1,
            justifyContent: "center",
            fontSize: "2.5rem",
            fontWeight: "bold"
          }}
        >
          Contact us
        </h2>
        <p className="text-center w-responsive mx-auto pb-5">
          We would love to hear from you. Get in touch with us by email.
        </p>
        <MDBRow>
          <MDBCol lg="5" className="lg-0 mb-4">
            <MDBCard>
              <MDBCardBody>
                <div className="form-header blue accent-1">
                  <h3 className="mt-2">
                    <MDBIcon icon="envelope" /> Write to us:
                  </h3>
                </div>
                <p className="dark-grey-text">
                  We'll write rarely, but only the best content.
                </p>
                <div className="md-form">
                  <MDBInput
                    icon="user"
                    label="Your name"
                    iconClass="grey-text"
                    type="text"
                    id="form-name"
                  />
                </div>
                <div className="md-form">
                  <MDBInput
                    icon="envelope"
                    label="Your email"
                    iconClass="grey-text"
                    type="text"
                    id="form-email"
                    onChange={e => {
                      this.handleEmailChange(e);
                    }}
                    value={this.state.email}
                  />
                </div>
                <div className="md-form">
                  <MDBInput
                    icon="tag"
                    label="Subject"
                    iconClass="grey-text"
                    type="text"
                    id="form-subject"
                    onChange={e => {
                      this.handleSubjectChange(e);
                    }}
                    value={this.state.subject}
                  />
                </div>
                <div className="md-form">
                  <MDBInput
                    icon="pencil-alt"
                    label="Your Message"
                    iconClass="grey-text"
                    type="textarea"
                    id="form-text"
                    onChange={e => {
                      this.handleBodyChange(e);
                    }}
                    value={this.state.body}
                  />
                </div>
                <div className="text-center">
                  <MDBBtn
                    color="light-blue"
                    onClick={e => {
                      this.sendEmail();
                    }}
                  >
                    Submit
                  </MDBBtn>
                </div>
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol lg="7">
            <div
              id="map-container"
              className="rounded z-depth-1-half map-container"
              style={{ height: "400px" }}
            >
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d76765.98321148289!2d-73.96694563267306!3d40.751663750099084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spl!2spl!4v1525939514494"
                title="This is a unique title"
                width="100%"
                height="100%"
                frameBorder="0"
                style={{ border: 0 }}
              />
            </div>
            <br />
            <MDBRow className="text-center">
              <MDBCol md="4">
                <MDBBtn tag="a" floating color="blue" className="accent-1">
                  <MDBIcon icon="map-marker-alt" />
                </MDBBtn>
                <p>New York, 94126</p>
                <p className="mb-md-0">United States</p>
              </MDBCol>
              <MDBCol md="4">
                <MDBBtn tag="a" floating color="blue" className="accent-1">
                  <MDBIcon icon="phone" />
                </MDBBtn>
                <p>+ 01 234 567 89</p>
                <p className="mb-md-0">Mon - Fri, 8:00-22:00</p>
              </MDBCol>
              <MDBCol md="4">
                <MDBBtn tag="a" floating color="blue" className="accent-1">
                  <MDBIcon icon="envelope" />
                </MDBBtn>
                <p>info@gmail.com</p>
                <p className="mb-md-0">sale@gmail.com</p>
              </MDBCol>
            </MDBRow>
          </MDBCol>
        </MDBRow>
      </section>
    );
  }
  sendEmail = () => {
    const email = this.state.email;
    const subject = this.state.subject;
    const body = this.state.body;

    const payload = {
      email: email,
      subject: subject,
      body: body
    };

    Axios.post(BASE_URL + "mailservice/mailto", payload)
      .then(res => {
        if (res.status == 200) {
          cogoToast.info(
            "Your email has been sent.We will get in touch with you soon!"
          );
        }
      })
      .catch(err => cogoToast.error("Error"));
  };
}

export default ContactPage;
