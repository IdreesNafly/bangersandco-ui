import React, { Component } from "react";
import MainNavbar from "../Components/MainNavbar";
import styled from "styled-components";
import axios from "axios";
import Loading from "../Components/Loading";
import pic from "../altpic.jpg";
import {
  MDBBtn,
  MDBCard,
  MDBIcon,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBCardText,
  MDBCol
} from "mdbreact";

const Wrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  margin-top: 2rem;
  height: 100vh;
`;
const Header = styled.div`
  display: flex;
  justify-content: center;
`;
const NoData = styled.div`
  display: flex;
  justify-content: center;
  flex: 1;
`;
const Image = styled.img`
  width: 15rem;
  height: 13rem;
`;

const Details = styled.div`
  display: flex;
  flex-direction: row;
  border-style: solid;
  margin: 2%;
  height: 13rem;
`;
const Title = styled.div`
  display: flex;
  flex: 2;
`;
const Body = styled.div`
  display: flex;
  flex: 4;
`;
const TagsWrapper = styled.div`
  display: flex;
  flex: 1;
`;
const Item = styled.div`
  display: flex;
  flex: 1;
  border-style: ridge;
`;

class Fleet extends Component {
  state = {
    vehicleData: null,
    isLoading: true,
    error: false
  };

  componentDidMount() {
    this.getvehicleData();
  }
  render() {
    return (
      <Wrapper>
        {/* <MainNavbar /> */}
        <Header className="h1-responsive font-weight-bold my-5">
          Our Fleet
        </Header>
        <Wrapper
          style={{
            flexDirection: "row",
            padding: "0 4rem 1rem 4rem",
            marginTop: 0,
            flexWrap: "wrap"
          }}
        >
          {this.state.loading ? (
            <Loading />
          ) : this.state.error ? (
            <NoData>Could Not Load Data </NoData>
          ) : (
            this.state.vehicleData &&
            this.state.vehicleData.map(vehicle => {
              return (
                // <Details>
                //   <Image src={vehicle.imagePath} alt={pic} />
                //   <Wrapper style={{ margin: "1%" }}>
                //     <Title>
                //       {vehicle.vehicleMake + " " + vehicle.vehicleModel}
                //     </Title>
                //     <Body>{vehicle.vehicleDescription}</Body>
                //     <TagsWrapper>
                //       <Item>{vehicle.mileage}</Item>
                //       <Item>{vehicle.fuelType}</Item>
                //       <Item>{vehicle.transmission}</Item>
                //       <Item>{vehicle.yom}</Item>
                //     </TagsWrapper>
                //   </Wrapper>
                // </Details>
                <MDBCol
                  style={{
                    maxWidth: "22rem",
                    minWidth: "20rem",
                    marginBottom: "1rem"
                  }}
                >
                  <MDBCard style={{ height: "500px" }}>
                    <MDBCardImage
                      className="img-fluid"
                      src={vehicle.imagePath}
                      waves
                    />
                    <MDBCardBody>
                      <MDBCardTitle>
                        {vehicle.vehicleMake +
                          " " +
                          vehicle.vehicleModel +
                          " (" +
                          vehicle.vehicleType +
                          ")"}
                      </MDBCardTitle>
                      <hr />
                      <MDBCardText>{vehicle.vehicleDescription}</MDBCardText>
                      <MDBCardText
                        style={{ color: "black", fontSize: "1.2rem" }}
                      >
                        {"LKR " + vehicle.vehicleRate + "  / Day"}
                      </MDBCardText>
                    </MDBCardBody>
                    <div className="rounded-bottom mdb-color lighten-3 text-center pt-3">
                      <ul className="list-unstyled list-inline font-small">
                        <li className="list-inline-item pr-2 white-text">
                          <MDBIcon icon="gas-pump" /> {vehicle.fuelType}
                        </li>
                        <li className="list-inline-item pr-2">
                          <a href="#!" className="white-text">
                            <MDBIcon icon="cogs" className="mr-1" />
                            {vehicle.transmission}
                          </a>
                        </li>
                        <li className="list-inline-item pr-2">
                          <a href="#!" className="white-text">
                            <MDBIcon icon="taxi" className="mr-1" />
                            {vehicle.yom}
                          </a>
                        </li>
                        {/* <li className="list-inline-item">
                          <a href="#!" className="white-text">
                            <MDBIcon fab icon="twitter" className="mr-1" />5
                          </a>
                        </li> */}
                      </ul>
                    </div>
                  </MDBCard>
                </MDBCol>
              );
            })
          )}
        </Wrapper>
      </Wrapper>
    );
  }

  getvehicleData() {
    // fetch("http://localhost:8080/api/vehicle/allvehicle", {
    //   mode: "no-cors"
    // }).then(data => {

    // });
    const token = localStorage.getItem("JWTToken");

    axios
      .get(`http://localhost:8080/api/vehicle/allvehicle`)
      .then(res => {
        const vehicledata = res.data;
        this.setState({
          vehicleData: vehicledata,
          isLoading: false,
          error: false
        });
      })
      .catch(error => {
        this.setState({
          isLoading: false,
          error: true
        });
      });
  }
}

const CardExample = () => {
  return (
    <MDBCol style={{ maxWidth: "22rem" }}>
      <MDBCard>
        <MDBCardImage
          className="img-fluid"
          src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg"
          waves
        />
        <MDBCardBody>
          <MDBCardTitle>Card title</MDBCardTitle>
          <MDBCardText>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </MDBCardText>
          <MDBBtn href="#">Click</MDBBtn>
        </MDBCardBody>
      </MDBCard>
    </MDBCol>
  );
};

export default Fleet;
