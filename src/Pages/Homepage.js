import React, { Component } from "react";
import styled from "styled-components";
import Footer from "../Components/Footer";
import Services from "../Components/OurServices";
import Testimonials from "../Components/Testimonials";
import Intro from "../Components/intro";
import MultiCarousel from "../Components/MultiCarousel";
import MainNavbar from "../Components/MainNavbar";
import PriceComparison from "../Components/PriceComparison";

const Wrappper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  overflow-x: hidden;
`;
const WebWrap = styled.div`
  display: flex;
  flex: 1;
  height: 50vh;
`;

class Homepage extends Component {
  state = {};
  componentDidMount() {
    // const data = JSON.parse(sessionStorage.getItem("userData"));
    // let data1 = data;
    // console.log(data1.data.Name);
    // console.log(data1.Name);
    // this.setState({ name: data1.data.Name });
  }
  render() {
    return (
      <Wrappper>
        <Intro />
        <div style={{ width: "100%" }}>
          <Services />
        </div>
        {/* <MultiCarousel /> */}
        <PriceComparison isUser={true} />
        <Testimonials />
        <Footer />
      </Wrappper>
    );
  }
}

export default Homepage;
