import React, { Component } from "react";
import { Formik, useField } from "formik";
import axios from "axios";
import styled from "styled-components";
import { Link, Redirect } from "react-router-dom";
import { message, Alert } from "antd";
import GoogleLogin from "react-google-login";
import {
  FacebookLoginButton,
  GoogleLoginButton
} from "react-social-login-buttons";

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex: 1;
  margin-top: 10%;
`;

const SignInBtn = styled.button`
  :hover {
    color: blue;
    cursor: pointer;
  }
`;
const MyTextInput = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={props.id || props.name}>{label}</label>
      <input className="text-input" {...field} {...props} />
      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
    </>
  );
};

export const HandleLogout = () => {
  localStorage.clear();
};
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthSuccess: false,
      isLoggedIn: false,
      loggedUser: [],
      useremail: null,
      userrole: null,
      isCustomer: false,
      error: "",
      loading: true,
      isFailed: false
    };
  }

  googleSignUp(res) {
    // const googleresponse = {
    //   Name: res.profileObj.name,
    //   email: res.profileObj.email,
    //   token: res.googleId,
    //   Image: res.profileObj.imageUrl,
    //   ProviderId: "Google"
    // };
    const Authentication = {
      email: res.profileObj.email,
      password: "",
      userRole: "Customer"
    };
    const User = {
      authentication: Authentication,
      email: res.profileObj.email,
      firstName: res.profileObj.givenName,
      lastName: res.profileObj.familyName,
      gender: "",
      contactNo: res.profileObj.googleId,
      address: "",
      age: ""
    };

    axios.post("http://localhost:8080/api/user/signup", User).then(result => {
      let googleResult = res;
      message.success("Login Success");
      localStorage.setItem("email", googleResult.profileObj.email);
      localStorage.setItem("JWTToken", googleResult.tokenId);
      localStorage.setItem("UserRole", "Customer");
      this.props.history.push("/");
    });
  }

  handleSubmit = values => {
    const login = {
      email: values.email,
      password: values.password
    };

    let self = this;
    axios
      .post("http://localhost:8080/authenticate/", login)
      .then(function(res) {
        const data = res.data;
        if (res.status === 200) {
          message.success("Login Success");
          localStorage.setItem("email", data.email);
          localStorage.setItem("JWTToken", data.jwtToken);
          localStorage.setItem("UserRole", data.userRole);
          if (data.userRole === "Customer") {
            self.setState({
              isCustomer: true
            });
          }
          self.setState({
            isAuthSuccess: true,
            isLoggedIn: true,
            loggedUser: login,
            loading: false
          });
        }
      })
      .catch(err => {
        console.log(err.response);
        this.setState({
          isFailed: true,
          error: err.status,
          loading: false
        });
      });
  };

  render() {
    const responseGoogle = response => {
      var res = response.profileObj;
      console.log(res);
      // debugger;
      this.googleSignUp(response);
    };

    return (
      <Wrapper>
        {this.state.isAuthSuccess && this.state.isCustomer ? (
          <Redirect
            to={{
              pathname: "/",
              state: {
                loggedUser: this.state.loggedUser,
                isLoggedIn: this.state.isLoggedIn
              }
            }}
          />
        ) : this.state.isAuthSuccess && this.state.isCustomer === false ? (
          <Redirect
            to={{
              pathname: "/admindashboard",
              state: {
                loggedUser: this.state.loggedUser,
                isLoggedIn: this.state.isLoggedIn
              }
            }}
          />
        ) : (
          ""
        )}
        <h1>Sign In</h1>
        <Formik
          initialValues={{ email: "", password: "" }}
          validate={values => {
            const errors = {};
            if (!values.email) {
              errors.email = "Required";
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = "Invalid email address";
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(false);
            this.handleSubmit(values);
            // setTimeout(() => {
            //   alert(JSON.stringify(values, null, 2));
            // }, 400);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting
            /* and other goodies */
          }) => (
            <form
              onSubmit={handleSubmit}
              style={{
                display: "flex",
                flexDirection: "column"
              }}
            >
              <MyTextInput
                type="email"
                name="email"
                label="Email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              <MyTextInput
                type="password"
                name="password"
                label="Password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              <div style={{ marginTop: "1rem" }}>
                {this.state.isFailed ? (
                  <Alert
                    message="Error"
                    description="Invalid Credentials"
                    type="error"
                    showIcon
                    closable
                  />
                ) : (
                  ""
                )}
              </div>
              <Link to={"/signup"}>Not Registered? Sign Up Here</Link>
              <SignInBtn
                style={{
                  borderRadius: "4px",
                  width: "98%",
                  alignSelf: "center",
                  height: "3rem",
                  background: "white"
                }}
                type="submit"
                onClick={e => this.handleSubmit}
              >
                Login
              </SignInBtn>

              <div>
                <GoogleLogin
                  clientId="84976794875-ihaa552elgf1hnr893g36l9428s844n9.apps.googleusercontent.com"
                  buttonText="Login with Google"
                  onSuccess={responseGoogle}
                  onFailure={responseGoogle}
                />
                {/* <GoogleLoginButton onClick={() => alert("Hello")} /> */}
                <FacebookLoginButton onClick={() => alert("Hello")} />
              </div>
            </form>
          )}
        </Formik>
      </Wrapper>
    );
  }
  getUserDetails(values) {
    let self = this;
    axios
      .get(`http://localhost:8080/api/user/getuser/${values.email}`, {
        headers: {
          Authorization: "Bearer " + values.jwtToken
        }
      })
      .then(function(res) {
        let data = res.data;
        // self.setstate({
        //   userrole: data.authentication.userRole
        // });
      });
  }
}

export default Login;
