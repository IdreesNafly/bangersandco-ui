import { Redirect } from "react-router-dom";
import React, { Component } from "react";

class Logout extends Component {
  state = {};

  render() {
    localStorage.clear();
    return <Redirect to="/" />;
  }
}

export default Logout;
