import React, { Component } from "react";
import MainNavbar from "../Components/MainNavbar";
import styled from "styled-components";
import Steppers from "../Components/Booking/steppers";
import axios from "axios";
class Rental extends Component {
  state = {};
  constructor() {
    super();
    this.state = {
      vehicleData: null,
      equipmentData: null,
      userData: null,
      isLoading: true,
      token: localStorage.getItem("JWTToken"),
      userId: localStorage.getItem("email")
    };
  }

  componentDidMount() {
    this.getvehicleData();
    this.getEquipmentData();
    this.getUserData();
  }
  render() {
    return (
      <div style={{ marginTop: "6rem" }}>
        {/* <MainNavbar /> */}
        {/* <div style={{ marginBottom: "4rem" }}>Rental Page</div> */}
        <Steppers
          vehicleData={this.state.vehicleData}
          equipmentData={this.state.equipmentData}
          token={this.state.token}
          user={this.state.userId}
          userData={this.state.userData}
        />
      </div>
    );
  }
  getvehicleData() {
    axios
      .get(`http://localhost:8080/api/vehicle/allvehicle`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        const vehicledata = res.data;
        this.setState({ vehicleData: vehicledata });
      });
  }

  getUserData() {
    axios
      .get(`http://localhost:8080/api/user/getuser/${this.state.userId}`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        const user = res.data.age;
        this.setState({ userData: user });
      });
  }

  getEquipmentData() {
    axios
      .get(`http://localhost:8080/api/equipments/all`, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        const equipmentData = res.data;
        this.setState({ equipmentData: equipmentData });
      });
  }
}

export default Rental;
