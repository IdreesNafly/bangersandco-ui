import React, { Component } from "react";
import { Formik, Form, useField } from "formik";
import * as Yup from "yup";
import styled from "styled-components";
import "../Stylesheets/styles-custom.css";
import "../Stylesheets/styles.css";
import { Link, Redirect } from "react-router-dom";
import { Alert } from "antd";
import {
  CheckCircleOutlined,
  DollarCircleTwoTone,
  StarTwoTone,
  CarTwoTone,
  PlusCircleTwoTone
} from "@ant-design/icons";
import cogoToast from "cogo-toast";

const axios = require("axios");

const MyTextInput = ({ label, ...props }) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input> and alse replace ErrorMessage entirely.
  const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={props.id || props.name}>{label}</label>
      <input className="text-input" {...field} {...props} />
      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
    </>
  );
};

const MyCheckbox = ({ children, ...props }) => {
  const [field, meta] = useField({ ...props, type: "checkbox" });
  return (
    <>
      <label className="checkbox">
        <input {...field} {...props} type="checkbox" />
        {children}
      </label>
      {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null}
    </>
  );
};

const StyledSelect = styled.select`
  color: var(--blue);
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
const StyledErrorMessage = styled.div`
  font-size: 12px;
  color: var(--red-600);
  width: 400px;
  margin-top: 0.25rem;
  &:before {
    content: "❌ ";
    font-size: 10px;
  }
  @media (prefers-color-scheme: dark) {
    color: var(--red-300);
  }
`;

const StyledLabel = styled.label`
  margin-top: 1rem;
`;

const Title = styled.div`
  display: flex;
  justify-content: center;
  font-size: 3.052rem;
  letter-spacing: -0.15rem;
  line-height: 1;
`;
const MySelect = ({ label, ...props }) => {
  // useField() returns [formik.getFieldProps(), formik.getFieldMeta()]
  // which we can spread on <input> and alse replace ErrorMessage entirely.
  const [field, meta] = useField(props);
  return (
    <>
      <StyledLabel htmlFor={props.id || props.name}>{label}</StyledLabel>
      <StyledSelect {...field} {...props} />
      {meta.touched && meta.error ? (
        <StyledErrorMessage>{meta.error}</StyledErrorMessage>
      ) : null}
    </>
  );
};

// And now we can use these
class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      address: "",
      contactNo: null,
      gender: null,
      formData: {},
      Authentication: false,
      registered: false
    };
  }

  handleSubmit = values => {
    const Authentication = {
      email: values.email,
      password: values.password,
      userRole: "Customer"
    };
    const User = {
      authentication: Authentication,
      email: values.email,
      firstName: values.firstName,
      lastName: values.lastName,
      gender: values.gender,
      contactNo: values.contactNo,
      address: values.address,
      age: values.age
    };

    let self = this;
    axios
      .post("http://localhost:8080/api/user/signup", User)
      .then(function(res) {
        if (res.status === 200) {
          self.setState({
            registered: true
          });
          cogoToast.success("User Registered Successfully");
        }
        axios
          .post("http://localhost:8080/authenticate/", Authentication)
          .then(function(res) {
            console.log(res, "res");
            localStorage.setItem("email", Authentication.email);
            localStorage.setItem("JWTToken", res.data.jwtToken);
            localStorage.setItem("UserRole", res.data.userRole);
            if (res && res.status && res.status === 200) {
              self.setState({
                Authentication: true
              });
            }
          });
      })
      .catch(error => {
        cogoToast.error(error.response.data.message);
      });
  };

  render() {
    return (
      <Wrapper>
        {this.state.Authentication ? <Redirect to="/" /> : ""}
        <Title>Sign Up!</Title>

        <Formik
          initialValues={{
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            confirmPassword: "",
            address: "",
            contactNo: null,
            acceptedTerms: false,
            gender: null
          }}
          validationSchema={Yup.object({
            firstName: Yup.string()
              .max(15, "Must be 15 characters or less")
              .required("Required"),
            lastName: Yup.string()
              .max(20, "Must be 20 characters or less")
              .required("Required"),
            password: Yup.string()
              .required("Required")
              .min(
                6,
                "Password too short.length should be minimum of 6 characters"
              )
              .max(20, "Password maximum character length is 20"),
            confirmPassword: Yup.string()
              .required("Required")
              .test("passwords-match", "Passwords must match", function(value) {
                return this.parent.password === value;
              }),
            email: Yup.string()
              .email("Invalid email addresss`")
              .required("Required"),
            acceptedTerms: Yup.boolean()
              .required("Required")
              .oneOf([true], "You must accept the terms and conditions."),
            gender: Yup.string()
              .oneOf(["male", "female"], "Invalid Gender")
              .required("Required")
          })}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values);
            setSubmitting(false);
            this.handleSubmit(values);
          }}
        >
          {({ isSubmitting }) => (
            <Form
              style={{
                display: "flex",
                flexDirection: "column",
                flex: "1",
                margin: "1rem 5rem 1rem 5rem"

                // alignItems: "center"
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row"
                }}
              >
                <Wrapper style={{ marginRight: "5rem" }}>
                  <MyTextInput
                    style={{ display: "flex", flexDirection: "column" }}
                    label="First Name"
                    name="firstName"
                    type="text"
                    placeholder=""
                  />
                  <MyTextInput
                    label="Email Address"
                    name="email"
                    type="email"
                    placeholder=""
                  />
                  <MyTextInput
                    label="Password"
                    name="password"
                    type="password"
                    placeholder=""
                  />
                  <MySelect label="Gender" name="gender">
                    <option value="">Select Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </MySelect>

                  <MyTextInput
                    label="Address"
                    name="address"
                    type="text"
                    placeholder=""
                  />
                </Wrapper>
                <Wrapper>
                  <MyTextInput
                    label="Last Name"
                    name="lastName"
                    type="text"
                    placeholder=""
                  />
                  <div style={{ height: "6rem" }}></div>
                  <MyTextInput
                    label="Confirm Password"
                    name="confirmPassword"
                    type="password"
                    placeholder=""
                  />
                  <MyTextInput
                    label="Contact No"
                    name="contactNo"
                    type="number"
                    placeholder=""
                  />
                  <MyTextInput
                    label="Age"
                    name="age"
                    type="date"
                    placeholder=""
                  />
                </Wrapper>
                <Wrapper
                  style={{
                    flex: 1,
                    padding: "3rem",
                    alignItems: "center",
                    border: "10px solid #4a5568",
                    margin: "3rem",
                    paddingTop: "1rem",
                    borderLeftStyle: "double",
                    borderTopStyle: "double"
                  }}
                >
                  <span>Why Register with Us?</span>
                  <img
                    style={{ width: "70%", height: "50%" }}
                    src="https://assets-ouch.icons8.com/preview/263/d597caab-0c13-49be-88d8-b39ab8db1ac0.png"
                    alt=""
                  />
                  <div>
                    <div style={{ margin: "0.5rem" }}>
                      {" "}
                      <DollarCircleTwoTone
                        style={{ marginRight: "1rem" }}
                      />{" "}
                      Price Match Guaranteed
                    </div>
                    <div style={{ margin: "0.5rem" }}>
                      {" "}
                      <StarTwoTone style={{ marginRight: "1rem" }} />
                      Manage Your Bookings
                    </div>
                    <div style={{ margin: "0.5rem" }}>
                      {" "}
                      <CarTwoTone style={{ marginRight: "1rem" }} />
                      Explore a wide range of vehicles
                    </div>
                    <div style={{ margin: "0.5rem" }}>
                      {" "}
                      <PlusCircleTwoTone style={{ marginRight: "1rem" }} />
                      Extend your bookings
                    </div>
                  </div>
                </Wrapper>
              </div>

              <MyCheckbox name="acceptedTerms">
                I accept the terms and conditions
              </MyCheckbox>
              <Link to={"/SignIn"}> Already have an account? Sign In here</Link>

              <button
                type="submit"
                disable={isSubmitting}
                onClick={e => this.handleSubmit}
              >
                Sign Up
              </button>
            </Form>
          )}
        </Formik>
      </Wrapper>
    );
  }
}

export default SignUp;
