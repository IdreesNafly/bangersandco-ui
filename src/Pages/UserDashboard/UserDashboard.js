import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import styled from "styled-components";
import {
  Avatar,
  Table,
  message,
  Modal,
  Button,
  Upload,
  Input,
  Form,
  Card,
  DatePicker
} from "antd";
//import { UploadOutlined } from "@ant-design/icons";
import axios from "axios";
import "react-datepicker/dist/react-datepicker.css";
import DatePickers from "react-datepicker";
import { addHours, startOfTomorrow, parseJSON } from "date-fns";
import { Link } from "react-router-dom";
import { Collapse } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBIcon
} from "mdbreact";

const { Panel } = Collapse;

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1;
  padding: 2rem 0 2rem 0;
  height: 100vh;
`;

const ProfileWrapper = styled.div`
  margin-top: 2rem;
  display: flex;
  flex-direction: column;
  background: #bfbfbf;
  color: black;
  justify-content: center;
  height: 90vh;
  width: 15vw;
`;

const DashboardWrapper = styled.div`
  display: flex;
  margin-top: 1rem;
  flex-direction: column;
  flex: 3;
  overflow-x: hidden;
  overflow-y: scroll;
  padding: 2rem;
`;
const Name = styled.div`
  display: flex;
  justify-content: center;
  font-size: 2rem;
  font-weight: bold;
  color: black;
`;

const ImgWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  flex: 1;
  justify-content: center;
`;
const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;
const DetailsWrapper = styled.div`
  flex-direction: column;
  display: flex;
  flex: 2;
  padding: 20px;
  align-items: start;
  font-variant-caps: all-petite-caps;
  font-size: 1.5rem;
`;
class UserDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedFile: null,
      visible: false,
      startDate: new Date(),
      endDate: null,
      returnDate: null,
      expiryDate: new Date()
    };
  }

  componentDidMount() {
    const user = localStorage.getItem("email");
    this.setState({
      useremail: user
    });
  }

  transformColumnData = () => {
    let data = this.props.bookingsData;
    let columns = [];
    if (data && data.length > 0) {
      const columnNames = Object.keys(data[0]);
      let col = {};
      let columnname;
      columnNames.map(column => {
        columnname = column.toUpperCase();
        col = {
          title: columnname,
          dataIndex: column,
          key: column
        };

        columns.push(col);
      });
    }
    return columns;
  };
  handleEndDate(date) {
    console.log(date, "aa");
    this.setState({
      returnDate: date
    });
  }

  onFinish = values => {
    console.log("Received values of form: ", values);
  };
  onChangeHandler = event => {
    console.log(event.target.files[0]);
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0
    });
  };

  render() {
    let userdata = this.props.userData;
    let data = this.props.bookingsData;
    let past, ongoing, futureBookings;
    if (data) {
      console.log(data);
      past = data.filter(data => data.bookingStatus === "Completed");
      ongoing = data.filter(data => data.bookingStatus === "InProgress");
      futureBookings = data.filter(
        data => data.bookingStatus === "Booking_Confirmed"
      );
    }

    let files = this.props.files;
    let images = this.props.images;
    return (
      <Wrapper>
        {/* <MainNavbar /> */}
        <ProfileWrapper>
          <ImgWrapper>
            <Avatar
              size={160}
              src="https://cdn0.iconfinder.com/data/icons/user-pictures/100/boy-2-512.png"
            />
          </ImgWrapper>

          {userdata ? (
            <DetailsWrapper>
              <Name>{userdata.firstName + " " + userdata.lastName}</Name>
              <div></div>
              <div>{userdata.firstName}</div>
              <div>{userdata.email}</div>
              <div>{userdata.contactNo} </div>
              <div>{userdata.gender} </div>
              <div>{userdata.address} </div>
              <div>
                {userdata.userDocuments.map(doc => {
                  return <div>{doc.docTitle + ": " + doc.docNo}</div>;
                })}
              </div>

              {/* <div>
                {this.props.files && this.props.files.length > 0
                  ? this.props.files.map(file => {
                      return <Avatar src={file.fileData} />;
                    })
                  : ""}
              </div> */}
            </DetailsWrapper>
          ) : (
            ""
          )}
        </ProfileWrapper>
        <DashboardWrapper>
          <Name>Your Bookings</Name>
          <Collapse
            bordered={false}
            defaultActiveKey={["1"]}
            className="site-collapse-custom-collapse"
            accordion
          >
            <Panel
              header="On Going Bookings"
              key="1"
              className="site-collapse-custom-panel"
            >
              <div>
                {/* <Name>OnGoing Bookings</Name> */}
                <Table
                  columns={this.transformColumnData()}
                  dataSource={ongoing}
                  expandedRowRender={this.expandedRowRender}
                />
              </div>
            </Panel>
            <Panel
              header="Past Bookings"
              key="2"
              className="site-collapse-custom-panel"
            >
              <div>
                {/* <Name>Past Bookings</Name> */}
                <Table
                  columns={this.transformColumnData()}
                  dataSource={past}
                  //expandedRowRender={this.expandedRowRender}
                />
              </div>
            </Panel>
            <Panel
              header="Future Bookings"
              key="3"
              className="site-collapse-custom-panel"
            >
              <div>
                <Table
                  columns={this.transformColumnData()}
                  dataSource={futureBookings}
                  expandedRowRender={this.expandedRowRenderFuture}
                />
              </div>{" "}
            </Panel>
          </Collapse>
          <div style={{ marginTop: "3rem" }}>
            {/* <Card accordion hoverable style={{}} actions={[]}></Card> */}
            <MDBContainer style={{ width: "50%" }}>
              <Name>Add User Documents</Name>
              <MDBRow>
                <MDBCol style={{ display: "flex", flex: 1 }}>
                  <MDBCard style={{ display: "flex", flex: 1 }}>
                    <MDBCardBody>
                      <form
                        style={{
                          padding: "2rem",
                          flexDirection: "column",
                          display: "flex"
                        }}
                        onSubmit={this.mySubmitHandler}
                      >
                        <p>Document Title</p>
                        <input type="text" onChange={this.nameChangeHandler} />
                        <p>Document No</p>
                        <input type="text" onChange={this.priceChangeHandler} />
                        <p>Expiry Date</p>
                        <DatePickers
                          selected={this.state.expiryDate}
                          onSelect={date => this.handleDateExpiry(date)}
                          dateFormat="MMMM d, yyyy"
                        />
                        <input
                          style={{ margin: "1.5rem" }}
                          type="file"
                          name="file"
                          onChange={this.onChangeHandler}
                        />
                        <button
                          style={{
                            backgroundColor: "#FFD000",
                            border: "1px solid #000"
                          }}
                          type="button"
                          //class="btn btn-success btn-block"
                          onClick={this.onClickHandler}
                        >
                          Upload
                        </button>
                      </form>
                    </MDBCardBody>
                  </MDBCard>
                </MDBCol>
              </MDBRow>
            </MDBContainer>
          </div>
        </DashboardWrapper>
        <div>
          <Modal
            title="Extend Booking"
            visible={this.state.visible}
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <form onSubmit={this.updateHandler}>
              <p>Your Booking will be extended till tomorrow 4:00 PM</p>
              <DatePickers
                selected={this.state.endDate}
                onSelect={date => this.handleEndDate(date)}
                maxDate={this.state.endDate}
                startDate={this.state.endDate}
                minDate={this.state.endDate}
                endDate={this.state.endDate}
                selectsEnd
                showTimeSelect
                timeFormat="HH:mm"
                timeIntervals={60}
                timeCaption="time"
                dateFormat="MMMM d, yyyy h:mm aa"
              />
            </form>
          </Modal>
        </div>
      </Wrapper>
    );
  }

  handleDateExpiry = e => {
    console.log(e);
    this.setState({
      expiryDate: e
    });
  };

  muSubmitHandler = event => {
    event.preventDefault();
    const title = this.state.title;
    const docno = this.state.docno;

    const payload = {
      equipmentName: title,
      costPerHour: docno
    };
    if (payload) {
      this.addEquipment(payload);
    }
  };
  nameChangeHandler = event => {
    console.log(event.target.value);
    this.setState({
      title: event.target.value
    });
  };
  priceChangeHandler = event => {
    console.log(event.target.value);
    this.setState({
      docno: event.target.value
    });
  };
  onClickHandler = () => {
    const data = new FormData();
    data.append("file", this.state.selectedFile);
    data.append("user", this.state.useremail);
    data.append("docno", this.state.docno);
    data.append("doctitle", this.state.title);
    data.append("expirydate", this.state.expiryDate);

    axios
      .post("http://localhost:8080/upload-file", data, {
        // receive two parameter endpoint url ,form data
      })
      .then(res => {
        // then print response status
        console.log(res.statusText);
      })
      .catch(err => {
        console.log(err);
      });
  };

  showModal = (id, extendedDate) => {
    this.setState({
      visible: true,
      bookingid: id,
      endDate: extendedDate
    });
  };
  handleOk = e => {
    this.props.extendBooking(this.state.bookingid, this.state.returnDate);
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };
  expandedRowRender = e => {
    const bookingid = e.bookingID;
    console.log(bookingid);
    const returnDate = e.endDate;
    const rt = this.state.returnDate;
    var extendedDate = new Date(returnDate);
    extendedDate.setDate(extendedDate.getDate() + 1);
    extendedDate.setHours(16, 0, 0, 0);
    return (
      <ButtonWrapper>
        {e.extendDate ? (
          ""
        ) : (
          <Button
            onClick={e => {
              this.showModal(bookingid, extendedDate);
            }}
          >
            Extend Booking
          </Button>
        )}
        <Link
          to={{
            pathname: `/checkout`,
            state: { bookingId: bookingid }
          }}
        >
          <Button>Return Vehicle</Button>
        </Link>
      </ButtonWrapper>
    );
  };

  expandedRowRenderFuture = e => {
    const bookingid = e.bookingID;
    const returnDate = e.endDate;
    var extendedDate = new Date(returnDate);
    extendedDate.setDate(extendedDate.getDate() + 1);
    extendedDate.setHours(16, 0, 0, 0);
    return (
      <ButtonWrapper>
        <Button
          onClick={e => {
            this.deleteVehicleById(bookingid);
          }}
        >
          Cancel Booking
        </Button>
      </ButtonWrapper>
    );
  };
}

export default withRouter(UserDashboard);
