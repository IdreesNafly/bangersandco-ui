import React, { Component } from "react";
import UserDashboard from "./UserDashboard";
import axios from "axios";
import { message } from "antd";
import cogoToast from "cogo-toast";

class UserDashboardContainer extends Component {
  state = {};

  componentDidMount() {
    const token = localStorage.getItem("JWTToken");
    const userid = localStorage.getItem("email");
    this.setState({
      token: token
    });
    this.getUserBookingsData(userid, token);
    this.getUserData(userid, token);
    this.getAllFilesByUser(userid);
    // this.downloadImage("download.jpg");
  }
  render() {
    console.log(this.state.files);
    return (
      <UserDashboard
        bookingsData={this.state.userBookingsData}
        userData={this.state.userData}
        extendBooking={this.extendBooking}
        files={this.state.files}
        image={this.state.image}
      />
    );
  }

  getUserBookingsData = (userid, token) => {
    let self = this;
    let data = [];
    axios
      .get(`http://localhost:8080/api/booking/getBooking/${userid}`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        if (res.status === 200) {
          data = res.data;
          self.setState({
            userBookingsData: data,
            loading: false,
            failed: false
          });
        } else {
          self.setState({
            loading: true,
            failed: true
          });
        }
      });
  };

  getUserData = (userid, token) => {
    let self = this;
    axios
      .get(`http://localhost:8080/api/user/getuser/${userid}`, {
        headers: {
          Authorization: "Bearer " + token
        }
      })
      .then(res => {
        let data = res.data;
        self.setState({
          userData: data
        });
      });
  };

  extendBooking = (id, extendDate) => {
    let self = this;
    const booking = {
      bookingID: id,
      extendDate: extendDate
    };
    axios
      .put(`http://localhost:8080/api/booking/extendbooking`, booking, {
        headers: {
          Authorization: "Bearer " + this.state.token
        }
      })
      .then(res => {
        let data = res.data;
        if (res.status === 200) {
          cogoToast.success("Your Booking has been Extended!");
          self.setState({
            userData: data
          });
        }
      })
      .catch(err => {
        if (err) {
          cogoToast.error(err.response.data.message);
        }
      });
  };

  downloadImage = filename => {
    let self = this;

    axios
      .put(`http://localhost:8080/download/${filename}/db`, null, {
        responseType: "blob"
      })
      .then(res => {
        const url = window.URL.createObjectURL(new Blob([res.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "file.pdf"); //or any other extension
        document.body.appendChild(link);
        link.click();
      });
  };
  getAllFilesByUser = user => {
    let self = this;
    axios.get(`http://localhost:8080/getdocuments/${user}`).then(res => {
      let data = res.data;
      self.setState({
        files: data
      });
    });
  };
}

export default UserDashboardContainer;
